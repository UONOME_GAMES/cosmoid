﻿using UnityEngine;
using System.Collections;

public class Plan : MonoBehaviour
{
		public Battery[] battery;
		public Mover[] mover;
		public Laser[] laser;
		public Enemy enemy;
		public GameObject player;
		[HideInInspector]

		public float nowLoop;
		public GameObject[] Bullets;
		[HideInInspector]
		public Pool pool;

		public void Start ()
		{
				enemy = this.GetComponent<Enemy> ();
				pool = GetComponent<Pool> ();
		}

		public virtual void Update ()
		{
				//PlanMain

		}

		public virtual void Init ()
		{
				ResetAllBattery ();
				StopAllCoroutines ();
				player = transform.parent.Find ("Player").gameObject;
				nowLoop = Time.time;
		}

		public virtual void ResetAllBattery ()
		{
				for (int i = 0; i < battery.Length; i++) {
						battery [i].Initialize ();
						battery [i].transform.position = this.transform.position;
						battery [i].transform.rotation = this.transform.rotation;
						battery [i].battery_ID = i;
				}
				for (int i = 0; i < mover.Length; i++) {
						mover [i].Initialize ();
						mover [i].transform.position = this.transform.position;
						mover [i].transform.rotation = this.transform.rotation;
				}
				for (int i = 0; i < laser.Length; i++) {
						laser [i].Initialize ();
						laser [i].transform.position = this.transform.position;
						laser [i].transform.rotation = this.transform.rotation;
				}
		}

		public virtual void TriggerOnWall (GameObject obj)
		{
		
		}

		public virtual void RaytoWallCollision (Vector2 c_position)
		{
		
		}

		public virtual void Invoked_Method ()
		{

		}

		public virtual void clearAllBullet ()
		{
				nowLoop = Time.time;
				Init ();
				foreach (GameObject obj in enemy.GetComponent<Pool>().array) {
						obj.GetComponent<Renderer>().enabled = false;
						obj.GetComponent<Animator> ().SetBool ("Created", false);
				}
		}

		public virtual float GetAngleToPlayer (Vector3 pos)
		{
				Vector3 direction = player.transform.position - pos;
				float dirRad = Mathf.Atan2 (direction.y, direction.x);
				return dirRad * Mathf.Rad2Deg;
		}

		public virtual float CalcTiming (float surplus, float gap)
		{
				float nowTime = Time.time - nowLoop;
				if (surplus != 0) {
						nowTime = nowTime % surplus - gap;
				}
				if (nowTime < 0) {
						nowTime -= Time.deltaTime;
				}
				return Mathf.Abs (nowTime);
		}
}
