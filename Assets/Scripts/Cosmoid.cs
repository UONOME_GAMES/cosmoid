﻿using UnityEngine;
using System.Collections;

public static class Cosmoid{

		private static int[] pluses = new int[]{0,3,7};

		private static string[] android = new string[]{"Shitataka","Kyoko"};
		private static string[] weapon = new string[]{"Missile","Bomb"};
	
	public static void Load(){

	}

		public static void SaveFrag(int _level, int _stage){
				int savefrag = pluses [_level - 1] + _stage +1;
				if (savefrag > 12) {savefrag = 12;}
				if (PlayerPrefs.GetInt ("Cosmoid_frag") < savefrag) {
						PlayerPrefs.SetInt ("Cosmoid_frag", savefrag);
						PlayerPrefs.Save ();
				}
	}

	public static int LoadFrag(){
				if (!PlayerPrefs.HasKey ("Cosmoid_frag")) {//フラグのキーがあるかどうか確認
						PlayerPrefs.SetInt ("Cosmoid_frag", 1);//無かったら初期化セーブ
						PlayerPrefs.Save ();
				}
				int _loadfrag = PlayerPrefs.GetInt ("Cosmoid_frag");
				return _loadfrag;
				//return 12;//Debug
	}

		public static int[] LoadRank(int _level,int _stage){
				int[] rank = new int[5];
				for (int i = 0; i < 12; i++) {
						for (int j = 0; j < 5; j++) {
								if (!PlayerPrefs.HasKey ("Cosmoid_rank"+i+"_"+j)) {//ランクのキーがあるかどうか確認
										PlayerPrefs.SetInt ("Cosmoid_rank"+i+"_"+j, 0);//無かったら初期化セーブ
										PlayerPrefs.Save ();
								}
						}
				}
				int num = pluses [_level - 1] + _stage - 1;
				for (int i = 0; i < 5; i++) {
						rank[i] = PlayerPrefs.GetInt("Cosmoid_rank"+num+"_"+i);
						//rank [i] = 0;//Debug
				}
				return rank;
		}

		public static string[] LoadRank_Prof(int _level,int _stage){
				string[] prof = new string[5];
				for (int i = 0; i < 12; i++) {
						for (int j = 0; j < 5; j++) {
								if (!PlayerPrefs.HasKey ("Cosmoid_prof" + i + "_" + j)) {//ランク情報のキーがあるかどうか確認
										PlayerPrefs.SetString ("Cosmoid_prof" + i + "_" + j, "Empty/Empty XXXX/XX/XX/XX:XX");//無かったら初期化セーブ
										PlayerPrefs.Save ();
								}
						}
				}
				int num = pluses [_level - 1] + _stage - 1;
				for (int i = 0; i < 5; i++) {
						prof[i] = PlayerPrefs.GetString("Cosmoid_prof"+num+"_"+i);
						//prof [i] = "Empty/Empty XXXX/XX/XX/XX:XX";//Debug
				}
				return prof;
	}

		public static bool SaveRank(int _level, int _stage, int score, Player p){
				int[] rank = LoadRank (_level, _stage);
				string[] prof = LoadRank_Prof (_level, _stage);
				int num = pluses [_level - 1] + _stage - 1;
				for (int i = 0; i < 5; i++) {
						if (score >= rank [i]) {
								int j = 3;
								while (j > i) {
										rank [j + 1] = rank [j];
										prof [j + 1] = prof [j];
										j--;
								}
								rank [i] = score;
								prof [i] = android [p.android] + "/" + weapon [p.weapon]+" "+ System.DateTime.Now.ToString ("yyyy/MM/dd/HH:mm:ss");
								for (j = 0; j < 5; j++) {
										PlayerPrefs.SetInt ("Cosmoid_rank" + num + "_" + j, rank[j]);
										PlayerPrefs.SetString ("Cosmoid_prof" + num + "_" + j, prof [j]);
								}
								PlayerPrefs.Save ();
								break;
						}
				}

		return true;
	}
}
