﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Select_Pane : Transition
{
		public GameObject title;
		public GameObject game;
		public GameObject cursor;
		public GameObject panel;
		public GameObject[] Panels = new GameObject[2];
		public Sprite[] android_Panes;
		public SpriteRenderer Android_Pane;
		private int _android = 0;
		public Sprite[] weapon_Panes;
		public SpriteRenderer Weapon_Pane;
		private int _weapon = 0;
		public Sprite[] level_Panes;
		public SpriteRenderer Level_Pane;
		private int _level = 1;
		public Sprite[] stage_Panes;
		public SpriteRenderer Stage_Pane;
		private int _stage = 1;
		private int _frag;
		private int[] _stageMax = new int[3];
		private int _levelMax;
		private int _nowCursor;
		public Text info;
		public string[] infotext;
		public string[] infostage;

		int[] pluses = new int[]{0,3,7};

		void Awake ()
		{
				_frag = Cosmoid.LoadFrag ();
				_stageMax [0] = Mathf.Clamp (_frag, -1, 3);
				_stageMax [1] = Mathf.Clamp (_frag - 3, -1, 4);
				_stageMax [2] = Mathf.Clamp (_frag - 7, -1, 5);
		}

		public override void Overriden_Update ()
		{
				if (Input.GetButtonDown ("Vertical") && Input.GetAxisRaw ("Vertical") > 0) {
						_nowCursor = 0;
				}
				if (Input.GetButtonDown ("Vertical") && Input.GetAxisRaw ("Vertical") < 0) {
						_nowCursor = 1;
				}
				cursor.transform.position = new Vector3 (0, Panels [_nowCursor].transform.position.y, 0);
				if (Input.GetButtonDown ("Z")) {
						switch (panel.GetComponent<Animator> ().GetInteger ("Panel_Number")) {
						case 0:
								panel.GetComponent<Animator> ().SetInteger ("Panel_Number", 1);
								info.text = "";
								break;
						case 1:
								GameObject obj = Instantiate (game) as GameObject;
								obj.GetComponent<Game_Pane> ().Initialize (_android, _weapon, _level, _stage);
								Exit ();
								break;
						}
				}
				if (Input.GetButtonDown ("X")) {
						switch (panel.GetComponent<Animator> ().GetInteger ("Panel_Number")) {
						case 0:
								Instantiate (title);
								Exit ();
								break;
						case 1:
								panel.GetComponent<Animator> ().SetInteger ("Panel_Number", 0);
								info.text = "Infomation";
								break;
						}
				}

				if (Input.GetButtonDown ("Horizontal") && Input.GetAxisRaw ("Horizontal") > 0) {
						switch (panel.GetComponent<Animator> ().GetInteger ("Panel_Number") * 2 + _nowCursor) {
						case 0://android
								_android++;
								if (_android > 1) {
										_android = 0;
								}
								Android_Pane.sprite = android_Panes [_android];
								info.text = infotext [_android];
								break;
						case 1://weapon
								_weapon++;
								if (_weapon > 1) {
										_weapon = 0;
								}
								Weapon_Pane.sprite = weapon_Panes [_weapon];
								info.text = infotext [2 + _weapon];
								break;
						case 2://level
								_level++;
								break;
						case 3://stage
								_stage++;
								break;
						}
						_level = ClampLevel (_level);
						_stage = ClampStage (_level, _stage);
						if (panel.GetComponent<Animator> ().GetInteger ("Panel_Number") * 2 + _nowCursor > 1) {
						info.text = infostage[pluses [_level - 1] + _stage - 1];
						}
						Level_Pane.sprite = level_Panes [_level - 1];
						Stage_Pane.sprite = stage_Panes [_stage - 1];
				}

				if (Input.GetButtonDown ("Horizontal") && Input.GetAxisRaw ("Horizontal") < 0) {
						switch (panel.GetComponent<Animator> ().GetInteger ("Panel_Number") * 2 + _nowCursor) {
						case 0://android
								_android--;
								if (_android < 0) {
										_android = 1;
								}
								Android_Pane.sprite = android_Panes [_android];
								info.text = infotext [_android];
								break;
						case 1://weapon
								_weapon--;
								if (_weapon < 0) {
										_weapon = 1;
								}
								Weapon_Pane.sprite = weapon_Panes [_weapon];
								info.text = infotext [2 + _weapon];
								break;
						case 2://level
								_level--;
								break;
						case 3://stage
								_stage--;
								break;
						}
						_level = ClampLevel (_level);
						_stage = ClampStage (_level, _stage);
						if (panel.GetComponent<Animator> ().GetInteger ("Panel_Number") * 2 + _nowCursor > 1) {
							info.text = infostage[pluses [_level - 1] + _stage - 1];
						}
						Level_Pane.sprite = level_Panes [_level - 1];
						Stage_Pane.sprite = stage_Panes [_stage - 1];
				}
		}

		int ClampStage (int levelValue, int stageValue)
		{
				if (stageValue > _stageMax [levelValue - 1]) {
						return 1;
				}
				if (stageValue < 1) {
						return _stageMax [levelValue - 1];
				}
				return stageValue;
		}

		int ClampLevel (int levelValue)
		{
				if (levelValue > _stageMax.Length) {
						return 1;
				}
				if (levelValue < 1) {
						levelValue = 3;
				}
				while (_stageMax [levelValue - 1] < 1) {
						levelValue--;
				}
				return levelValue;
		}
}	