﻿using UnityEngine;
using System.Collections;

public class Title_Pane : Transition {
	public GameObject[] Buttons;

	public GameObject[] Next_Panes;

	public GameObject cursor;

	private int _nowButton;

	public override void Overriden_Update(){
		if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") > 0) {
			//Debug.Log ("up");
			_nowButton--;
		}
		if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") < 0) {
			//Debug.Log ("down");
			_nowButton++;
		}
		_nowButton = Mathf.Clamp (_nowButton, 0, Buttons.Length - 1);
		//Debug.Log (_nowButton);
		cursor.transform.localPosition = Buttons [_nowButton].transform.localPosition;
		if (Input.GetButtonDown("Z")) {
			if(_nowButton <= 2){
				Instantiate(Next_Panes[_nowButton]);
				Exit();
			}else{
				Application.Quit();
			}
		}
	}

}
