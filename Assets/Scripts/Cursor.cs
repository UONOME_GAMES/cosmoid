﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class Cursor : MonoBehaviour {

	private Animator _animator;

	private AudioSource SE_Source;
	
	public AudioClip Click_SE;

	public AudioClip Enter_SE;

	public AudioClip Exit_SE;

	// Use this for initialization
	void Start () {
		SE_Source = GameObject.Find ("SE_Player").GetComponent<AudioSource> ();
		_animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") > 0){
			_animator.SetTrigger("Up");
			SE_Source.PlayOneShot(Click_SE);
		}
		else if(Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") < 0){
			_animator.SetTrigger("Down");
			SE_Source.PlayOneShot(Click_SE);
		}
		else if(Input.GetButtonDown("Horizontal") && Input.GetAxisRaw("Horizontal") > 0){
			_animator.SetTrigger("Right");
			SE_Source.PlayOneShot(Click_SE);
		}
		else if(Input.GetButtonDown("Horizontal") && Input.GetAxisRaw("Horizontal") < 0){
			_animator.SetTrigger("Left");
			SE_Source.PlayOneShot(Click_SE);
		}
		if (Input.GetButtonDown ("Z")) {
			SE_Source.PlayOneShot(Enter_SE);
		}
		if (Input.GetButtonDown ("X")) {
			SE_Source.PlayOneShot(Exit_SE);
		}
	}
}
