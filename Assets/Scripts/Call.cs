﻿using UnityEngine;
using System.Collections;

public class Call : MonoBehaviour {

		public AudioClip SE;

	// Use this for initialization
	void Start () {
				GameObject.Find ("SE_Player").GetComponent<AudioSource> ().PlayOneShot(SE);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

		public void Call_exit(int phase){
				if (phase == 0) {
						GameObject.Find ("Game_Pane(Clone)").GetComponent<Game_Pane>().StartGame ();
						Destroy (this.gameObject);
				} else {
						GameObject.Find ("Game_Pane(Clone)").transform.Find ("Enemy").GetComponent<Enemy> ().EndGame ();
						Destroy (this.gameObject);
				}
		}
}
