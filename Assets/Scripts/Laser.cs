﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class Laser : MonoBehaviour {
	
	public float RollSpeed;

	private float wait_Time;

	private float standBy_Time;

	private float irradiate_Time;

	private float shootTime;

	public Sprite standBy;

	public Sprite irradiate;

	private SpriteRenderer SR;

	private RaycastHit2D RC;

	private Plan P;

	private AudioSource SE_Source;

	public AudioClip SE;

	// Use this for initialization
	void Start () {
		SR = GetComponent<SpriteRenderer> ();
		P = transform.parent.GetComponent<Plan> ();
		SE_Source = GameObject.Find ("SE_Player").GetComponent<AudioSource> ();
		Initialize ();
	}
	
	// Update is called once per frame
	void Update () {
		if (SR.enabled) {
			if (RollSpeed != 0){
				transform.Rotate (new Vector3(0,0,RollSpeed * Time.deltaTime));
			}
			if (Time.time - shootTime < standBy_Time) {//stand By
				if(!SR.sprite.name.Equals(standBy.name)){
					SR.sprite = standBy;
					//Debug.Log ("StandBy");
				}
			}else if(Time.time - shootTime < standBy_Time + irradiate_Time){//irradiate
				if(!SR.sprite.name.Equals(irradiate.name)){
					SR.sprite = irradiate;
					SE_Source.PlayOneShot(SE);
					//Debug.Log ("Irradiate");
				}
				RC = Physics2D.Raycast(transform.position,transform.right,transform.localScale.x,1<<LayerMask.NameToLayer("Wall"));
				if(RC.collider != null){
					P.RaytoWallCollision(RC.point);
					//Debug.Log ("Collision Wall!");
				}
				RC = Physics2D.Raycast(transform.position,transform.right,transform.localScale.x,1<<LayerMask.NameToLayer("Player"));
				if(RC.collider != null){
					RC.collider.gameObject.GetComponent<Player>()._HPslider.value -= 0.1f;
					RC.collider.gameObject.GetComponent<Animator>().SetTrigger("Damage");
					RC.collider.gameObject.GetComponent<Player> ().Damage();
					//Debug.Log ("Collision Player!");
				}
			}else{//finish irradiate
				SR.enabled = false;
				//Debug.Log ("Finish");
			}
		}else{
			if(wait_Time > 0){
				if(Time.time - shootTime > standBy_Time + irradiate_Time + wait_Time){//finishwaitTime
					shootTime = Time.time;
					SR.enabled = true;
				}
			}
		}
	}

	public void ShootLaser(){
		shootTime = Time.time;
		SR.enabled = true;
	}

	public void SetLaser(float S, float I,float L){
		standBy_Time = S;
		irradiate_Time = I;
		transform.localScale = new Vector3 (L, 1, 1);
	}

	public void SetLaser(float S, float I,float L,float W){
		SetLaser (S, I, L);
		wait_Time = W;
	}

	public void Initialize(){
		transform.rotation = Quaternion.Euler (new Vector3 (0, 0, 0));
		transform.localScale = new Vector3 (1, 1, 1);
		RollSpeed = 0;
		wait_Time = 0;
		SR.enabled = false;
	}
}
