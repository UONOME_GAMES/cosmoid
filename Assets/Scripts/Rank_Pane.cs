﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Rank_Pane : Transition {

	public GameObject nextPane;

	public GameObject cursor;

	public Sprite[] level_Panes;

	public SpriteRenderer Level_Pane;

	private int _level = 1;

	public Sprite[] stage_Panes;

	public SpriteRenderer Stage_Pane;

	public GameObject[] Panels = new GameObject[2];

	private int _stage = 1;

	private int _frag;

	private int[] _stageMax = new int[3];

	private int _levelMax;

	private int _nowCursor;


	public Text rank;

	public Text prof;


	void Awake(){
			_frag = Cosmoid.LoadFrag ();
			_stageMax [0] = Mathf.Clamp (_frag, -1, 3);
			_stageMax [1] = Mathf.Clamp (_frag - 3, -1, 4);
			_stageMax [2] = Mathf.Clamp (_frag - 7, -1, 5);
	}

	public override void Start ()//\nが改行
	{
		base.Start ();
		GameObject.Find ("BGM_Player").GetComponent<AudioSource> ().Stop ();
				SetText ();
	}

	public override void Overriden_Update (){
		if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") > 0) {
				_nowCursor = 0;
		}
		if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") < 0) {
				_nowCursor = 1;
		}
		cursor.transform.position = new Vector3 (0, Panels[_nowCursor].transform.position.y,0);

		if (Input.GetButtonDown("Horizontal") && Input.GetAxisRaw("Horizontal") > 0) {
				switch(_nowCursor){
				case 0://level
						_level++;
						break;
				case 1://stage
						_stage++;
						break;
				}
				_level = ClampLevel(_level);
				_stage = ClampStage(_level, _stage);
				Level_Pane.sprite = level_Panes[_level - 1];
				Stage_Pane.sprite = stage_Panes[_stage - 1];
						SetText ();
		}

		if (Input.GetButtonDown("Horizontal") && Input.GetAxisRaw("Horizontal") < 0) {
				switch(_nowCursor){
				case 0://level
						_level--;
						break;
				case 1://stage
						_stage--;
						break;
				}
				_level = ClampLevel(_level);
				_stage = ClampStage(_level, _stage);
				Level_Pane.sprite = level_Panes[_level - 1];
				Stage_Pane.sprite = stage_Panes[_stage - 1];
						SetText ();
		}

		if (Input.GetButtonDown ("X")) {
			Instantiate(nextPane);
			Exit ();
		}
	}

	int ClampStage(int levelValue, int stageValue){
			if (stageValue > _stageMax [levelValue - 1]) {
					return 1;
			}
			if (stageValue < 1) {
					return _stageMax[levelValue - 1];
			}
			return stageValue;
	}

	int ClampLevel(int levelValue){
			if (levelValue > _stageMax.Length) {
					return 1;
			}
			if (levelValue < 1) {
					levelValue =3;
			}
			while(_stageMax [levelValue - 1] < 1){
					levelValue--;
			}
			return levelValue;
	}
				
	void SetText(){
				int[] load_rank = Cosmoid.LoadRank (_level, _stage);
				string[] load_prof = Cosmoid.LoadRank_Prof (_level, _stage);
				rank.text = load_rank [0] + "\n" + load_rank [1] + "\n" + load_rank [2] + "\n" + load_rank [3] + "\n" + load_rank [4];
				prof.text = load_prof [0] + "\n" + load_prof [1] + "\n" + load_prof [2] + "\n" + load_prof [3] + "\n" + load_prof [4];
	}

}
