﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(CircleCollider2D))]
public class Bullet : MonoBehaviour
{
		public Sprite[] sprites;
		public Color[] COLORS;
		public float[] SCALES;
		[HideInInspector]
		public float count;
		public float lifeTime = 20;
		public float speed;
		public int reflection;
		public bool move;
		private Vector2 min;
		private Vector2 max;
		[HideInInspector]
		public int battery_ID;
		private Animator animator;
		// Use this for initialization
		void Awake ()
		{
				GetComponent<Rigidbody2D>().velocity = (transform.right * speed);
				GetComponent<Rigidbody2D>().Sleep ();
				animator = GetComponent<Animator> ();
				min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
				max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));
		}
		// Update is called once per frame
		void Update ()
		{
				if (GetComponent<Renderer>().enabled) {
						if (!GetComponent<Rigidbody2D>().IsAwake ()) {
								GetComponent<Rigidbody2D>().WakeUp ();
						}
						if (Time.time - count > lifeTime) {
								GetComponent<Renderer>().enabled = false;
								animator.SetBool ("Created", false);
						}
				}
				GetComponent<Collider2D>().isTrigger = true;
				if (transform.position.x > max.x || transform.position.x < min.x ||
				  transform.position.y > max.y || transform.position.y < min.y) {
						GetComponent<Collider2D>().isTrigger = true;
				} else {
						if (reflection > 0) {
								GetComponent<Collider2D>().isTrigger = false;
						}
				}
				if (move) {
						GetComponent<Rigidbody2D>().WakeUp ();
						transform.rotation = Quaternion.AngleAxis (Mathf.Atan2 (GetComponent<Rigidbody2D>().velocity.y, GetComponent<Rigidbody2D>().velocity.x) * Mathf.Rad2Deg, Vector3.forward);
				} else {
						GetComponent<Rigidbody2D>().Sleep ();
				}
		}

		void InitBullet ()
		{
				int rnd = Random.Range (0, COLORS.Length);
				GetComponent<SpriteRenderer> ().color = COLORS [rnd];
				//Debug.Log (GetComponent<SpriteRenderer> ().color);
				rnd = Random.Range (0, SCALES.Length);
				gameObject.transform.localScale = new Vector2 (SCALES [rnd], SCALES [rnd]);
				animator.Rebind ();
				animator.SetBool ("Created", true);
		}

		public void shooted (Transform t, float s, bool m, int r, int ID, float l)
		{
				count = Time.time;
				InitBullet ();
				this.transform.position = t.position;
				this.move = m;
				this.reflection = r;
				this.lifeTime = l;
				battery_ID = ID;
				burst (t, s);
		}

		public void burst (Transform t, float s)
		{
				this.transform.rotation = t.rotation;
				this.speed = s;
				GetComponent<Rigidbody2D>().velocity = transform.right * speed;
		}

		void OnTriggerEnter2D (Collider2D c)
		{//collision player
				if (c.gameObject.name.Equals ("Player") && GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("Enable")) {
						//Debug.Log ("Collision Player!");
						c.gameObject.GetComponent<Player> ()._HPslider.value 
								-= this.gameObject.transform.localScale.x * 1.5f - c.gameObject.GetComponent<Player> ().weapon * 0.2f;
						c.gameObject.GetComponent<Player> ().Damage ();
						c.gameObject.GetComponent<Animator> ().SetTrigger ("Damage");
						GetComponent<Renderer>().enabled = false;
						GetComponent<Animator> ().SetBool ("Created", false);
				}
		}

		void OnCollisionEnter2D (Collision2D c)
		{//collision wall
				//Debug.Log ("Collision Wall!");
				transform.parent.GetComponent<Plan> ().TriggerOnWall (this.gameObject);
				if (reflection == 0) {
						this.gameObject.GetComponent<Collider2D>().isTrigger = true;
				} else {
						reflection--;
						this.gameObject.GetComponent<Collider2D>().isTrigger = false;
				}
		}
}
