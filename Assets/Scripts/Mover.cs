using UnityEngine;
using System.Collections;

/// <summary>
/// Moverクラス
/// SetLoopで設定した感覚でSet○○で指定した動きをするクラス <br>
/// 指定できる動き:円(Oval), 線(Line), 正葉線(Leaf), リサージュ(Lissajous), 内サイクロイド(Hypocycloid) <br>
/// 使用条件:小オブジェクトを持ち,Inspector上でpositionに設定する必要あり
/// </summary>
public class Mover : MonoBehaviour {
	//定数
	public const int INFINITY = -1;
	//動き方
	public const int OVAL = 0;
	public const int LINE = 1;
	public const int LEAF = 2;
	public const int LISSAJOUS = 3;
	public const int HYPOCYCLOID = 4;
	
	//繰り返し方
	public const int GO_BACK_PLUS = 2;
	public const int PLUS = 1;
	public const int MINUS = -1;
	public const int GO_BACK_MINUS = -2;
	
	//緩急の付け方
	public const int DEFAULT = 0;
	public const int DIM2_EASE_IN = 1;
	public const int DIM2_EASE_OUT = 2;
	public const int DIM3 = 3;
	
	//変数
	[HideInInspector]
	public int kind_of_loop; //繰り返し方
	public float loop_time; //1ループの時間
	public float interval_time; //1ループごとの間隔
	[HideInInspector]
	public int loops; //ループする回数
	
	///動き方の設定用
	[HideInInspector]
	public float[] moveValues = new float[5]; //自由に使える変数
	[HideInInspector]
	public int kind_of_move; //動き方
	public int kind_of_lerp; //緩急の付け方
	
	///記録(記憶)用変数 ※初期化必須
	public float percentage; //パーセンテージ記録用
	[HideInInspector]
	public float one_sec_perce; //1フレームに増える(減る)パーセンテージ
	[HideInInspector]
	public float loop_rec; //今現在のループ開始時の記録
	[HideInInspector]
	public bool isInterval;
	public bool isWorking; //そもそもいま動かしてるのかどうか
	public GameObject position; //マーカー本体
	public GameObject homing;
	
	public virtual void Start(){
		
	}
	
	public virtual void Initialize(){
		SetOval(0,360);
		transform.rotation = Quaternion.Euler (transform.right);
		transform.position = transform.parent.position;
		kind_of_loop = PLUS;
		kind_of_lerp = DEFAULT;
		isWorking = false;
		isInterval = false;
		homing = null;
	}
	
	public void SetLoop(float LoopTime,float IntervalTime){
		isWorking = false;
		interval_time = IntervalTime;
		loop_time = LoopTime-IntervalTime;
		if (loop_time == 0) {
			one_sec_perce = 1 / interval_time;
		}else{
			one_sec_perce = 1 / loop_time;
		}
	}
	
	public void StartLoop(int KindOfLoop,int Loops,int KindOfLerp){
		loops = Loops;
		kind_of_loop = KindOfLoop;
		kind_of_lerp = KindOfLerp;
		if (kind_of_loop > 0) {
			percentage = 0;
		} else {
			percentage = 1;
			one_sec_perce *= -1;
		}
		isWorking = true;
		loop_rec = Time.time;
	}

	void InvokeStart(){

		}
	
	// Update is called once per frame
	public virtual void Update () {
		if (isWorking) {
			Up_Working();
			if (!isInterval) {
				Up_Not_Interval();
			} else {
				Up_Interval();
			}
		}else{
			Up_Not_Working();
		}
	}
	
	public virtual void Up_Not_Interval(){
		if(homing != null){
			transform.position = homing.transform.position;
			transform.rotation = homing.transform.rotation;
		}
		Move ();
	}

	public virtual void Up_Interval(){
		if ((Time.time - loop_rec) >= (loop_time + interval_time)) {
			isInterval = false;
			loops--;
			loop_rec = Time.time;
			if(loops == 0){
				isWorking = false;
			}else{
				switch(kind_of_loop){
				case PLUS:
					percentage = 0;
					break;
				case GO_BACK_PLUS:
					one_sec_perce *= -1;
					break;
				case MINUS:
					percentage = 1;
					break;
				case GO_BACK_MINUS:
					one_sec_perce *= -1;
					break;
				}
			}
		}
	}
	
	public virtual void Up_Working(){
		
	}
	
	public virtual void Up_Not_Working(){
		
	}
	
	/// <summary>
	/// <para>distance: 中心点からの距離</para>
	/// <para>width_degree: 扇の中心角 (0 to 360)</para>
	/// <para></para>
	/// </summary>
	public void SetOval(float distance,float width_degree){
		kind_of_move = OVAL;
		moveValues [0] = distance;
		moveValues [1] = width_degree;
	}
	
	void CalcOval(float deg){
		float nowRad = ((deg * moveValues [1])-(moveValues [1] / 2)) * Mathf.Deg2Rad;
		position.transform.localRotation = Quaternion.Euler(new Vector3(0,0,nowRad * Mathf.Rad2Deg));
		float x = Mathf.Cos (nowRad) * moveValues [0];
		float y = Mathf.Sin (nowRad) * moveValues [0];
		position.transform.localPosition = new Vector3 (x, y, 0);
	}
	
	/// <summary>
	/// point 1 から 2 の直線上を動く
	/// <para>point 1,2: そのまんま</para>
	/// <para></para>
	/// </summary>
	public void SetLine(Vector2 point1,Vector2 point2){
		kind_of_move = LINE;
		moveValues [0] = point1.x;
		moveValues [1] = point1.y;
		moveValues [2] = point2.x;
		moveValues [3] = point2.y;
	}
	
	void CalcLine(float deg){
		position.transform.position = Vector2.Lerp (
			new Vector2 (moveValues [0], moveValues [1]), 
			new Vector2 (moveValues [2], moveValues [3]), 
			deg
			);
		position.transform.localRotation = transform.localRotation;
	}
	
	/// <summary>
	/// <para>size: 正葉線の大きさ</para>
	/// <para>pieces: 葉っぱの数</para>
	/// <para></para>
	/// </summary>
	public void SetLeaf(float size,int pieces,float a,float b){
		kind_of_move = LEAF;
		moveValues [0] = size;
		moveValues [1] = pieces;
		moveValues [2] = a;
		moveValues [3] = b;
	}
	
	void CalcLeaf(float deg){
		float nowRad = deg * 360 * Mathf.Deg2Rad;
		position.transform.localRotation = Quaternion.Euler(new Vector3(0,0,nowRad * Mathf.Rad2Deg));
		float r = moveValues[2] + Mathf.Cos (moveValues[1] * nowRad / moveValues[3]);
		float x = moveValues[0] * r * Mathf.Cos(nowRad);float y = moveValues[0] * r * Mathf.Sin(nowRad);		
		position.transform.localPosition = new Vector3(x,y,0);
	}
	
	/// <summary>
	/// <para>size: 曲線のサイズ</para>
	/// <para>a,b: 振動数</para>
	/// <para></para>
	/// </summary>
	public void SetLissajous(float size,float a,float b,float d){
		kind_of_move = LISSAJOUS;
		moveValues [0] = size;
		moveValues [1] = a;
		moveValues [2] = b;
		moveValues [3] = d;
	}
	
	void CalcLissajous(float deg){
		float nowRad = deg * 360 * Mathf.Deg2Rad;
		position.transform.localRotation = Quaternion.Euler(new Vector3(0,0,nowRad * Mathf.Rad2Deg));
		float x = moveValues [0] * Mathf.Cos(nowRad * moveValues [1]);float y = moveValues [0] * Mathf.Sin( moveValues [2] * (nowRad + moveValues [3]));
		position.transform.localPosition = new Vector3(x,y,0);
	}
	
	/// <summary>
	/// <para>distance: distance from center</para>
	/// <para>width_degree: oval degree width (0 to 360)</para>
	/// <para>bullet_number: </para> 
	/// <para></para>
	/// </summary>
	public void SetHypocycloid(float size,int m){
		kind_of_move = HYPOCYCLOID;
		moveValues [0] = size;
		moveValues [1] = m;
	}
	
	void CalcHypocycloid(float deg){
		float nowRad = deg * 360 * Mathf.Deg2Rad;
		position.transform.localRotation = Quaternion.Euler(new Vector3(0,0,nowRad * Mathf.Rad2Deg));
		float M = moveValues [0] / moveValues [1];
		float x = (moveValues [0] - M) * Mathf.Cos(nowRad) + M * Mathf.Cos((moveValues [0] - M) / M * nowRad);
		float y = (moveValues [0] - M) * Mathf.Sin(nowRad) - M * Mathf.Sin((moveValues [0] - M)/ M * nowRad);
		position.transform.localPosition = new Vector3(x,y,0);
	}
	
	public void Move(){
		CalcPos ();
		percentage += one_sec_perce * Time.deltaTime;
		if (percentage > 1) {
			isInterval = true;
			percentage = 1;
		} else if (percentage < 0) {
			isInterval = true;
			percentage = 0;
		}
	}

		public void CalcPos(){
			switch (kind_of_move) {
			case OVAL:
					CalcOval(CalcLerp (percentage));
					break;
			case LINE:
					CalcLine(CalcLerp (percentage));
					break;
			case LEAF:
					CalcLeaf (CalcLerp (percentage));
					break;
			case LISSAJOUS:
					CalcLissajous(CalcLerp (percentage));
					break;
			case HYPOCYCLOID:
					CalcHypocycloid(CalcLerp (percentage));
					break;
			}
		}
	
	float CalcLerp(float t){
		switch (kind_of_lerp) {
		case DIM2_EASE_IN:
			return Mathf.Pow (t,2);
		case DIM2_EASE_OUT:
			return t*(2-t);
		case DIM3:
			return Mathf.Pow(t,2) * (3 - 2 * t);
		case DEFAULT:
			return t;
		default:
			return t;
		}
	}
}