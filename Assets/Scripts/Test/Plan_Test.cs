﻿using UnityEngine;
using System.Collections;

public class Plan_Test : Plan
{
		//----dictionary---
		//switch(enemy._nowPlan){
		//case :
		//
		//break;
		//}
		public override void Init ()
		{
				StopAllCoroutines ();
				base.Init ();
				switch (enemy._nowPlan) {
				case 1://Tactics_1/Reddy
						battery [0].isWorking = true;
						battery[0].prefab = Bullets[0];
						battery[0].SetShootDegree(Battery.ROCK_ON,0);
						battery[0].SetWays(13,270);
						break;
				case 2://Tactics_5/Reddy
						battery [0].isWorking = true;
						battery [0].prefab = Bullets [1];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [1].prefab = Bullets [2];
						battery [1].SetOval (0, 27);
						battery [1].SetWays (19, 340);
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						break;
				case 3://Tactics_8/Morphy
						for (int i = 0; i < 3; i++) {
								mover [i].transform.Rotate (new Vector3 (0, 0, i * 120));
								mover [i].SetOval (3.7f, 360);
								mover [i].SetLoop (7, 0);
								mover [i].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
								battery [i].isWorking = true;
						}
						battery [0].prefab = Bullets [3];
						battery [0].homing = mover [0].position;
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].SetWays (2, 30);
						battery [1].prefab = Bullets [4];
						battery [1].homing = mover [1].position;
						battery [1].SetShootDegree (Battery.RELATIVE, 0);
						battery [2].prefab = Bullets [5];
						battery [2].homing = mover [2].position;
						battery [2].SetShootDegree (Battery.ABSOLUTE, -90);
						break;
				case 4://Tactics_19/Morphy
						battery [0].prefab = Bullets [6];
						battery [0].transform.position = enemy.transform.position;
						battery [0].SetHypocycloid (1.3f, 6);
						battery [0].SetShootDegree (Battery.RELATIVE, 15);
						battery [0].SetLoop (1.8f, 1.1f);
						battery [1].prefab = Bullets [7];
						battery [1].SetLine (new Vector2 (-5, 6), new Vector2 (5, 6));
						battery [1].SetShootDegree (Battery.ABSOLUTE, -90);
						battery [1].SetLoop (2, 2);
						//battery [1].SetSpeed (2, 3.5f);
						//battery [1].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 5://Tactics_10/Kateru
						battery [0].transform.localPosition = new Vector2 (0, 0);
						battery [0].prefab = Bullets [8];
						battery [0].SetShootDegree (Battery.ROCK_ON, 180);
						battery [0].SetWays (30, 330);
						battery [1].prefab = Bullets [9];
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						battery [2].prefab = Bullets [10];
						battery [2].SetShootRandom (Battery.ROCK_ON, 60, 180);
						battery [1].transform.position = new Vector2 (0, 0);
						battery [2].transform.position = new Vector2 (0, 0);
						break;
				case 6://Tactics_32/Kateru
						battery [1].prefab = Bullets [11];
						battery [1].SetShootRandom (Battery.ABSOLUTE, 120, 90);
						battery [1].SetWays (2, 180);
						battery [1].SetLoop (1.6f, 1.2f);
						battery [1].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [0].prefab = Bullets [12];
						battery [0].SetShootRandom (Battery.ABSOLUTE, 120, 90);
						battery [0].SetWays (2, 180);
						battery [0].SetLoop (2, 2);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 7://Tactics_21/Yui
						battery [0].isWorking = true;
						battery [0].prefab = Bullets [13];
						battery [0].SetOval (4.5f, 270);
						battery [0].transform.Rotate (new Vector3 (0, 0, 90));
						battery [0].transform.localPosition = new Vector2 (0, -1);
						battery [0].SetShootDegree (Battery.RELATIVE, 170);
						battery [0].SetLoop (3, 0);
						battery [0].StartLoop (Mover.GO_BACK_PLUS, Mover.INFINITY, Mover.DEFAULT);
						battery [1].isWorking = true;
						battery [1].prefab = Bullets [14];
						battery [1].SetOval (3.8f, 270);
						battery [1].transform.Rotate (new Vector3 (0, 0, 90));
						battery [1].transform.localPosition = new Vector2 (0, -1);
						battery [1].SetShootDegree (Battery.RELATIVE, -175);
						battery [1].SetLoop (2, 0);
						battery [1].StartLoop (Mover.GO_BACK_MINUS, Mover.INFINITY, Mover.DEFAULT);
						break;
				case 8://Tactics_20/Yui
						battery [0].SetHypocycloid (3f, 10);
						battery [0].transform.Rotate (new Vector3 (0, 0, 110));
						battery [0].prefab = Bullets [15];
						battery [0].SetShootDegree (Battery.RELATIVE, 160);
						battery [0].SetLoop (1.8f, 0);
						battery [1].SetHypocycloid (3f, 5);
						battery [1].transform.Rotate (new Vector3 (0, 0, 110));
						battery [1].prefab = Bullets [16];
						battery [1].SetShootRandom (Battery.RELATIVE, 45, 115);
						battery [1].SetLoop (1, 0);
						break;
				case 9://Tactics_24/Mai
						battery [0].prefab = Bullets [18];
//						battery [0].SetShootRandom (Battery.RELATIVE, 5, 180);
//						battery [0].SetOval (2, 180);
//						battery [0].transform.rotation = Quaternion.Euler (0, 0, 180);
//						battery [0].transform.position = new Vector2 (4, 0);
						battery [1].prefab = Bullets [1];
//						battery [1].SetShootRandom (Battery.RELATIVE, 5, 180);
//						battery [1].SetOval (2, 180);
//						battery [1].transform.position = new Vector2 (-4, 0);
//						battery [0].SetSpeed (3, 1);
						//battery [0].SetLoop (1.6f, 1, 24);
//						battery [1].SetSpeed (3, 1);
						//battery [1].SetLoop (1.6f, 1, 24);
//						battery [0].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DEFAULT);
//						battery [1].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DEFAULT);
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						break;
				case 10://Tactics_35/Mai
						battery [0].prefab = Bullets [19];
						battery [1].prefab = Bullets [20];
						battery [0].SetOval (0, 360);
						battery [1].SetOval (1, 360);
						battery [0].SetLoop (0.7f, 0);
						//battery [0].SetShootRolling (1, -90);
						battery [1].SetLoop (0.5f, 0);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [1].StartLoop (Mover.MINUS, -1, Mover.DEFAULT);
						battery [2].prefab = Bullets [17];
						battery [2].SetOval (0.5f, 360);
						battery [2].SetLoop (1, 0);
						break;
				}
		}

		IEnumerator Plan1(){
				battery [0].prefab.GetComponent<Bullet> ().speed = 2.8f;
				battery [0].Shoot ();
				battery [0].Play_SE ();
				yield return new WaitForSeconds (0.05f);
				battery [0].prefab.GetComponent<Bullet> ().speed = 3.3f;
				battery [0].Shoot ();
				battery [0].Play_SE ();
				yield return new WaitForSeconds (0.05f);
				battery [0].prefab.GetComponent<Bullet> ().speed = 4;
				battery [0].Shoot ();
				battery [0].Play_SE ();
				yield break;
		}

		IEnumerator Plan2(){
			for (int i = 0; i < 4; i++) {
				yield return new WaitForSeconds (0.3f);
				battery [1].SetPosition ();
				battery [1].Shoot ();
						battery [1].Play_SE ();
			}
				yield break;
		}

		IEnumerator Plan3(){
				for(int i = 0;i < 25;i ++){
						yield return new WaitForSeconds (0.07f);
						battery [0].Shoot ();
						battery [0].Play_SE ();
				}
				yield break;
		}

		IEnumerator Plan4(){
				for (int i = 0; i < 20; i++) {
						battery [0].percentage = 0.05f * i;
						//Debug.Log (battery [0].percentage);
						battery [0].CalcPos ();
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.04f);
			}
			float rnd = Random.Range (0, 45);
				foreach (GameObject bul in pool.GetBulletsByCountOver(0.03f,1f,0)) {
					bul.GetComponent<Bullet> ().move = true;
					bul.transform.Rotate (new Vector3 (0, 0, rnd - 22.5f));
					bul.GetComponent<Bullet> ().burst (bul.transform, bul.GetComponent<Bullet> ().speed);
			}
				yield break;
		}

		IEnumerator Plan5(){
//				battery [1].transform.position = new Vector2 (0, 0);
//				battery [2].transform.position = new Vector2 (0, 0);
				battery [1].Shoot ();
				for (int i = 0; i < 5; i++) {
						battery [0].prefab.GetComponent<Bullet> ().speed = 2 + i * 0.4f;
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.14f);
				}
				yield break;
		}

		IEnumerator Plan6(){
				for (int i = 0; i < 4; i++) {
						battery [1].Shoot ();
						battery [1].Play_SE ();
						yield return new WaitForSeconds (0.1f);
				}
				yield break;
		}

		IEnumerator Plan8(){
				battery [0].transform.position = player.transform.position;
				battery [0].transform.rotation = Battery.LookAt2D (player, this.gameObject);
				for (int i = 0; i < 100; i++) {
						battery [0].percentage = 0.01f * i;
						battery [0].CalcPos ();
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.01f);
				}
				foreach (GameObject bullet in pool.GetBulletsByCountOver(0,0)) {
						bullet.GetComponent<Bullet> ().move = true;
						bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
				}
				battery [0].Play_SE ();
				yield break;
		}

		IEnumerator Plan9(){
				for (int i = 10; i > 0; i--) {
						battery [0].SetWays (2, i * 10);
						for (int j = 0; j < 5; j++) {
								battery [0].prefab.GetComponent<Bullet> ().speed = j * 0.8f;
								battery [0].Shoot ();
						}
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.1f);
				}
				//for (int i = 0; i < 20; i++) {
						yield return new WaitForSeconds (0.05f);
						battery [1].Shoot ();
						battery [1].Play_SE ();
				//}
				yield break;
		}

		public override void Update ()
		{
				switch (enemy._nowPlan) {
				case 1://Tes
						if (CalcTiming (0.7f, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan1");
						}
						break;
				case 2:
						if (CalcTiming (2.2f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
						}
						if (CalcTiming (2.2f, 0.1f) <= Time.deltaTime) {
								battery [1].homing = pool.GetBulletsByCountOver (0, 1, 0) [0];
								StartCoroutine ("Plan2");
								//battery [1].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
						}
						if (nowLoop - Time.time > 2.2f) {
								nowLoop = Time.time;
						}
						break;
				case 3:
						for (int i = 0; i < 3; i++) {
								battery [i].SetPosition ();
						}
						if (CalcTiming (0.1f,0) <= Time.deltaTime) {
								battery [1].Shoot ();
						}
						if (CalcTiming (0.13f,0) <= Time.deltaTime) {
								battery [2].Shoot ();
						}
						if (CalcTiming (2.6f,0) <= Time.deltaTime) {
								StartCoroutine ("Plan3");
						}
						break;
				case 4:
						if (CalcTiming (1.8f, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan4");
						}
						if (CalcTiming (1, 0) <= Time.deltaTime) {
								for (int i = 0; i < 20; i++) {
										battery [1].prefab.GetComponent<Bullet> ().speed = 1.5f + Mathf.PingPong (i * 0.2f,1);
										battery [1].percentage = 0.05f * i;
										battery [1].CalcPos();
										battery [1].Shoot ();
								}
								battery [1].Play_SE ();
						}
						break;
				case 5:
						battery [1].transform.position = new Vector2 (0, 0);
						battery [2].transform.position = new Vector2 (0, 0);
						if (CalcTiming (1.3f, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan5");
						}
						if (CalcTiming (0.1f, 0) <= Time.deltaTime) {
								battery [2].Shoot ();
								battery [2].Play_SE ();
						}
						break;
				case 6:
						if (CalcTiming (1.6f, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan6");
						}
						if (CalcTiming (2f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						break;
				case 7:
						if (CalcTiming (0.1f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						if (CalcTiming (0.05f, 0) <= Time.deltaTime) {
								battery [1].Shoot ();
								battery [1].Play_SE ();
						}
						break;
				case 8:
						if (CalcTiming (2, 1) <= Time.deltaTime) {
								battery [1].transform.position = player.transform.position;
//								battery [1].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
//								Invoke ("Invoked_Method", 0.7f);
								for (int i = 0; i < 40; i++) {
										battery [1].percentage = i * 0.025f;
										battery [1].CalcPos ();
										battery [1].Shoot ();
								}
								battery [1].Play_SE ();
						}
						if (CalcTiming (4, 1) <= Time.deltaTime) {
								StartCoroutine ("Plan8");
						}
						break;
				case 9:
						if (CalcTiming (2.5f, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan9");
						}
						break;
				case 10:
						if (CalcTiming (0.0625f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
						}
						if (CalcTiming (0.08f, 0) <= Time.deltaTime) {
								battery [1].Shoot ();
						}
						if (CalcTiming (2, 0) <= Time.deltaTime) {
								battery [2].transform.rotation = Battery.LookAt2D (player, battery [2].gameObject);
								battery [2].SetShootDegree (Battery.RELATIVE, 200);
								for (int i = 0; i < 25; i++) {
										battery [2].percentage = 0.04f * i;
										battery [2].CalcPos ();
										battery [2].Shoot ();
								}
								battery [2].SetShootDegree (Battery.RELATIVE, -200);
								for (int i = 0; i < 25; i++) {
										battery [2].percentage = 0.04f * i;
										battery [2].CalcPos ();
										battery [2].Shoot ();
								}
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(2.5f,0)) {
								bullet.GetComponent<Bullet> ().move = false;
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(2.5f,1)) {
								bullet.GetComponent<Bullet> ().move = false;
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(3.3f,0)) {
								bullet.GetComponent<Bullet> ().move = true;
								bullet.transform.Rotate (new Vector3 (0, 0, Random.Range (0, 360)));
								bullet.GetComponent<Bullet> ().burst (bullet.transform, 1);
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(3.3f,1)) {
								bullet.GetComponent<Bullet> ().move = true;
								bullet.transform.Rotate (new Vector3 (0, 0, Random.Range (0, 360)));
								bullet.GetComponent<Bullet> ().burst (bullet.transform, 1);
						}
						break;
				}
		}

		public override void Invoked_Method ()
		{
				switch (enemy._nowPlan) {
				case 8:

						break;
				}
		}

		public override void RaytoWallCollision (Vector2 c_position)
		{
		
		}
}
