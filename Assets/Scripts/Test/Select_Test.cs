﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Select_Test : Transition {

	public GameObject title;

	public GameObject game;

	public GameObject cursor;

	public GameObject panel;

	public GameObject[] Panels = new GameObject[2];


	public Sprite[] android_Panes;

	public SpriteRenderer Android_Pane;

	private int _android = 0;

	public Sprite[] weapon_Panes;
	
	public SpriteRenderer Weapon_Pane;

	private int _weapon = 0;

	private int _nowCursor;

	public Text info;

	public string[] infotext;

	void Awake(){

	}

	public override void Overriden_Update(){
		if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") > 0) {
			_nowCursor = 0;
		}
		if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") < 0) {
			_nowCursor = 1;
		}
		cursor.transform.position = new Vector3 (0, Panels[_nowCursor].transform.position.y,0);
		if(Input.GetButtonDown ("Z")){
			switch(panel.GetComponent<Animator>().GetInteger("Panel_Number")){
			case 0:
				panel.GetComponent<Animator>().SetInteger("Panel_Number",1);
				info.text = "";
				break;
			case 1:
				GameObject obj = Instantiate(game) as GameObject;
				obj.GetComponent<Game_Pane>().Initialize(_android,_weapon,1,1);
				Exit();
				break;
			}
		}
		if (Input.GetButtonDown ("X")) {
			switch(panel.GetComponent<Animator>().GetInteger("Panel_Number")){
			case 0:
				Instantiate(title);
				Exit ();
				break;
			case 1:
				panel.GetComponent<Animator>().SetInteger("Panel_Number",0);
				info.text = "Infomation";
				break;
			}
		}

		if (Input.GetButtonDown("Horizontal") && Input.GetAxisRaw("Horizontal") > 0) {
			switch(panel.GetComponent<Animator>().GetInteger("Panel_Number")*2+_nowCursor){
			case 0://android
				_android++;
				if(_android > 1){_android = 0;}
				Android_Pane.sprite = android_Panes[_android];
				info.text = infotext[_android];
				break;
			case 1://weapon
				_weapon++;
				if(_weapon > 1){_weapon = 0;}
				Weapon_Pane.sprite = weapon_Panes[_weapon];
				info.text = infotext[2 + _weapon];
				break;
			default:
				info.text = "";
				break;
			}
		}

		if (Input.GetButtonDown("Horizontal") && Input.GetAxisRaw("Horizontal") < 0) {
			switch(panel.GetComponent<Animator>().GetInteger("Panel_Number")*2+_nowCursor){
			case 0://android
				_android--;
				if(_android < 0){_android = 1;}
				Android_Pane.sprite = android_Panes[_android];
				info.text = infotext[_android];
				break;
			case 1://weapon
				_weapon--;
				if(_weapon < 0){_weapon = 1;}
				Weapon_Pane.sprite = weapon_Panes[_weapon];
				info.text = infotext[2 + _weapon];
				break;
			default:
				info.text = "";
				break;
			}
		}
	}
}	