﻿using UnityEngine;
using System.Collections;

public class End_Pane : Transition {

	public float waitTime;

	public GameObject nextPane;

	private float startTime;

	public override void Start (){
		base.Start ();
		startTime = Time.time;
	}

	public override void Overriden_Update (){
		if (Time.time - startTime >= waitTime) {
			EndPane();
		}else if (Input.GetButtonDown("X")) {
			EndPane();
		}
	}

	void EndPane(){
		if (GameObject.Find (nextPane.name + "(Clone)") == null) {
			Instantiate (nextPane);
		}
		Exit ();
	}

}
