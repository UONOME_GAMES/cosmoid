﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Enemy : MonoBehaviour {

	private ParticleSystem particle;

	public ParticleSystem explode;

	public GameObject Call_Neutralize;

	public float HP;

	public int planMax;

	public Animator cut_In;

	public float BonusTime;

	public int ClearScore;
	
	private Slider _HPslider;

	[HideInInspector]
	public float FinishTime;

	[HideInInspector]
	public GameObject _player;

	[HideInInspector]
	public int _nowPlan;

	private AudioSource SE_Source;
	
	public AudioClip Tactics_SE;

	public AudioClip Explode_SE;

	private Text _NowPlan;

	// Use this for initialization
	void Awake () {
		gameObject.name = "Enemy";
		particle = GetComponent<ParticleSystem> ();
		//transform.parent = GameObject.Find ("Game_Pane").transform;//type Debug
		transform.parent = GameObject.Find ("Game_Pane(Clone)").transform;
		_HPslider = transform.parent.Find("Canvas").Find("Enemy_HP").GetComponent<Slider>();
		_HPslider.maxValue = HP;_HPslider.minValue = 0;
		_HPslider.value = HP;
		_NowPlan = transform.parent.Find("Canvas").Find("Enemy_Tactics").GetComponent<Text>();
		SE_Source = GameObject.Find ("SE_Player").GetComponent<AudioSource> ();
	}

	public void NextPlan(){
		_nowPlan++;
		_NowPlan.text ="" + _nowPlan;
		if(_nowPlan == 1){FinishTime = Time.time;}
		GetComponent<Plan> ().Init ();
		if (_nowPlan < planMax) {
			cut_In.SetTrigger ("Next");
			_HPslider.value = HP;
			particle.Play();
			SE_Source.PlayOneShot(Tactics_SE);
		}else if (_nowPlan > planMax) {//clear
			GameObject p = Instantiate (explode.gameObject) as GameObject;
			p.transform.parent = this.transform;
			_HPslider.value = HP;
			FinishTime = Time.time - FinishTime;
			SE_Source.PlayOneShot(Explode_SE);
		}else{//final tactics
			cut_In.SetTrigger ("Final");
			_HPslider.value = HP;
			particle.Play();
			SE_Source.PlayOneShot(Tactics_SE);
		}
	}

	public void Damage(float damage){
		_HPslider.value -= damage;
		GetComponent<Plan> ().clearAllBullet ();
	}

	void Update(){
		if (_nowPlan >= 1 && _nowPlan <= planMax) {
			_HPslider.value -= Time.deltaTime;
		}
		if (_HPslider.value <= _HPslider.minValue) {
			NextPlan();
		}
		if (transform.Find (explode.gameObject.name + "(Clone)") != null) {
			if(!transform.Find (explode.gameObject.name + "(Clone)").gameObject.GetComponent<ParticleSystem>().isPlaying){
				GetComponent <Plan>().clearAllBullet();
				Destroy (transform.Find (explode.gameObject.name + "(Clone)").gameObject);
				Instantiate (Call_Neutralize);
			}
		}
	}

		public void EndGame(){
				transform.parent.GetComponent<Game_Pane>().StartTalk(false,GetComponentInParent<Game_Pane>()._android * 12 + GetComponentInParent<Game_Pane>()._gameNum);
		}
}
