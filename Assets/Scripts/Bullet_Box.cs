﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class Bullet_Box : MonoBehaviour {

	void OnTriggerExit2D(Collider2D c) {
		//c.gameObject.SetActive (false);
		c.GetComponent<Animator>().SetBool("Created",false);
	}
}
