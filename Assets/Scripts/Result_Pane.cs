﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Result_Pane : Transition
{
		public GameObject Base;
		public GameObject Penalty;
		public GameObject T_Bonus;
		public GameObject ND_Bonus;
		public GameObject Total;
		private int _counter;
		private int nextNum;
		private bool _clear;
		public GameObject[] nextPanel = new GameObject[3];

		public int StartResult (bool C, int B, float PHP, int T)
		{
				Base.GetComponentInChildren<Text> ().text = "" + B;
				Penalty.GetComponentInChildren<Text> ().text = "-" + (int)(B * (1 - PHP));
				T_Bonus.GetComponentInChildren<Text> ().text = "" + T;
				ND_Bonus.GetComponentInChildren<Text> ().text = "" + 7000 * (int)PHP;
				Total.GetComponentInChildren<Text> ().text = "" + ((int)(B * PHP) + T + 7000 * (int)PHP);
				_clear = C;
				return ((int)(B * PHP) + T + 7000 * (int)PHP);
		}
		// Update is called once per frame
		public override void Overriden_Update ()
		{
				if (Input.GetButtonDown ("Z")) {
						switch (_counter) {
						case 0://Base
								Base.GetComponentInParent<Animator> ().SetTrigger ("Pop_up");
								break;
						case 1://Penalty
								Penalty.GetComponentInParent<Animator> ().SetTrigger ("Pop_up");
								break;
						case 2://TimeBonus
								T_Bonus.GetComponentInParent<Animator> ().SetTrigger ("Pop_up");
								break;
						case 3://NoDamageBonus
								ND_Bonus.GetComponentInParent<Animator> ().SetTrigger ("Pop_up");
								break;
						case 4://Total
								Total.GetComponentInParent<Animator> ().SetTrigger ("Pop_up");
								break;
						case 5://isClear&RankIn
								break;
						case 6://Quit
				//GameObject.Find("Game_Pane").GetComponent<Transition>().Exit();//Debug
				//Game_Pane game_pane = GameObject.Find("Game_Pane").GetComponent<Game_Pane>();//typeDebug
								GameObject.Find ("Game_Pane(Clone)").GetComponent<Transition> ().Exit ();
								Game_Pane game_pane = GameObject.Find ("Game_Pane(Clone)").GetComponent<Game_Pane> ();
								if (!_clear || game_pane._stage < game_pane._level + 2) {
										nextNum = 0;//continue
								} else if (game_pane._stage == 5) {
										nextNum = 2;//ending
								} else {
										nextNum = 1;//endpanel
								}
								GameObject obj = Instantiate (nextPanel [nextNum]) as GameObject;
								switch (nextNum) {
								case 0://continue
										obj.GetComponent<Continue_Pane> ().Setting (_clear, game_pane._level, game_pane._stage, game_pane._android, game_pane._weapon);
										break;
								case 1://endpanel
										obj.transform.Find ("EndPanel").GetComponent<Animator> ().SetInteger ("PanelNumber", game_pane._level);
										break;
								case 2://ending

										break;
								}
								Exit ();
								break;
						}
						_counter++;
				}
		}
}
