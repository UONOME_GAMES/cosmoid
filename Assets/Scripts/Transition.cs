﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class Transition : MonoBehaviour {

	[HideInInspector]
	public SpriteRenderer[] _renderers;
	
	public bool bluring;

	[HideInInspector]
	public Animator animator;

	public AudioClip BGM;

	public bool BGM_loop;
	
	// Use this for initialization
	public virtual void Start () {
		_renderers = GetComponentsInChildren<SpriteRenderer> ();
		animator = GetComponent<Animator> ();
		if (BGM != null) {
			AudioSource au = GameObject.Find ("BGM_Player").GetComponent<AudioSource>();
			if(au != null && (!au.clip.name.Equals(BGM.name) || !au.isPlaying)){
				au.Stop();
				au.clip = BGM;
				au.loop = BGM_loop;
				au.Play ();
			}
		}
	}
	
	// Update is called once per frame
	public virtual void Update () {
		if (!bluring) {
			Overriden_Update();
		}else{
			Bluring(GetComponent<SpriteRenderer>().color.a);
		}
	}

	public virtual void Bluring(float a){
		foreach (SpriteRenderer rndr in _renderers) {
			rndr.color = new Color(rndr.color.r, rndr.color.g, rndr.color.b, a);
		}
	}

	public virtual void Overriden_Update(){

	}

	public virtual void BlurFinished(){
		Destroy(this.gameObject);
	}

	public virtual void Exit(){
		bluring = true;
		animator.SetTrigger ("Blur_Out");
	}
}
