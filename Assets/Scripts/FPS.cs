﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FPS : MonoBehaviour {
	public float Timing;

	private Text _textObject;

	// Use this for initialization
	void Start () {
		_textObject = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time % Timing < Time.deltaTime) {
			_textObject.text = (1/Time.deltaTime).ToString("f2") + "fps";
		}
	}
}
