﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool : MonoBehaviour {

	public int BulletMax = 300;
	
	public float bullet_LifeTime = 20;
	
	[HideInInspector]
	public List<GameObject> array = new List<GameObject>();
	
	private int lastNo = 0;
	


	// Use this for initialization
	void Start () {
	
	}

	public GameObject GetBullet(GameObject prefab){
		GameObject obj = null;
		
		for (int i = 0; i < array.Count; i++) {
			obj = array[i].gameObject;
			if(!obj.GetComponent<Renderer>().enabled){
				lastNo = i;
				//obj.SetActive(true);
				obj.GetComponent<Renderer>().enabled = true;
				SetBullet(prefab,obj);
				obj.GetComponent<Animator>().SetBool("Created",true);
				return obj;
			}
		}
		if (array.Count > BulletMax) {
			if(lastNo >= array.Count){
				lastNo = 0;
			}
			obj = array[lastNo].gameObject;
			//obj.SetActive(true);
			obj.GetComponent<Renderer>().enabled = true;
			SetBullet(prefab,obj);
			obj.GetComponent<Animator>().SetBool("Created",true);
			return obj;
		}
		obj = Instantiate (prefab) as GameObject;
		obj.transform.parent = transform;//enemy
		obj.GetComponent<Renderer>().enabled = true;
		array.Add (obj);
		lastNo = array.Count - 1;
		obj.GetComponent<Animator>().SetBool("Created",true);
		return obj;
	}

	void SetBullet(GameObject prefab, GameObject obj){
		obj.GetComponent<CircleCollider2D> ().radius = prefab.GetComponent<CircleCollider2D> ().radius;
		obj.GetComponent<SpriteRenderer> ().sprite = prefab.GetComponent<SpriteRenderer> ().sprite;
	}

	public GameObject[] GetBulletsByCountOver(float count,int ID){
		List<GameObject> objects = new List<GameObject>();
		foreach (GameObject obj in array) {
			if(obj.GetComponent<Renderer>().enabled && obj.GetComponent<Bullet>().battery_ID == ID){
				if(Time.time - obj.GetComponent<Bullet>().count >= count){
					objects.Add (obj);
				}
			}
		}
		return objects.ToArray();
	}

	public GameObject[] GetBulletsByCountOver(float min, float max,int ID){
		List<GameObject> objects = new List<GameObject>();
		foreach (GameObject obj in array) {
			if(obj.GetComponent<Renderer>().enabled && obj.GetComponent<Bullet>().battery_ID == ID){
				float count = Time.time - obj.GetComponent<Bullet>().count;
				if(count >= min && count <= max){
					objects.Add (obj);
				}
			}
		}
		return objects.ToArray();
	}
	
	public GameObject[] GetBulletsByCountEquals(float count,int ID){
		List<GameObject> objects = new List<GameObject>();
		foreach (GameObject obj in array) {
			if(obj.GetComponent<Renderer>().enabled && obj.GetComponent<Bullet>().battery_ID == ID){
				float cnt = Time.time - obj.GetComponent<Bullet>().count - count;
				if(cnt > 0 && cnt < Time.deltaTime){
					objects.Add (obj);
				}
			}
		}
		return objects.ToArray();
	}
	
	
	public void Release(GameObject obj){
		//obj.SetActive (false);
		obj.GetComponent<Renderer>().enabled = false;
		obj.GetComponent<Animator>().SetBool("Created",false);
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i<array.Count; i++) {
			if(array[i].gameObject.GetComponent<Renderer>().enabled){
				if(Time.time - array[i].GetComponent<Bullet>().count > bullet_LifeTime){
					Release(array[i].gameObject);
				}
			}
		}
	}
}
