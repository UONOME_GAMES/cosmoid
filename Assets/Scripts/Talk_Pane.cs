﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Talk_Pane : Transition {
	public TextAsset txt_file;
	public string[] serifs;
	public bool before;
	private string Title = "";
	private string Player_Name = "";
	private string Enemy_Name = "";
	private string Text = "";
	private int now_Serif_Num = -1;
	public GameObject Name_Object;
	public GameObject Text_Object;
	public GameObject Title_Object;
	public GameObject Player_stand;
	public GameObject Enemy_stand;
	public GameObject Call_Warning;
	
	// Use this for initialization
	public override void Start () {
		base.Start ();
		Player_stand.GetComponent<Animator>().SetBool ("Shadow_On",true);
		Enemy_stand.GetComponent<Animator>().SetBool ("Shadow_On",true);
		LoadFile ();
	}
	
	public void LoadFile(){
		string[] str_array = txt_file.text.Split ("/"[0]);
		Title = str_array[0];
		Player_Name = str_array [1];
		Enemy_Name = str_array [2];
		serifs = str_array [3].Split(">"[0]);
	}
	
	void ReadSerif(int serifNum){
		if (serifNum < serifs.Length) {
			string[] serif = serifs[serifNum].Split (":"[0]);
			int face_Num = 0;
			switch (serif[1]){
			case "喜":
				face_Num = 1;
				break;
			case "怒":
				face_Num = 2;
				break;
			case "哀":
				face_Num = 3;
				break;
			case "呆":
				face_Num = 4;
				break;
			case "驚":
				face_Num = 5;
				break;
			case "戦":
				face_Num = 6;
				break;
			}
			if(serif[0].Equals ("!")){
				Text = serif[2];
				Player_stand.GetComponent<Animator>().SetInteger("Face",face_Num);
				Player_stand.GetComponent<Animator>().SetBool ("Shadow_On",false);
				Player_stand.transform.position = new Vector3(0,0,0);
				Enemy_stand.GetComponent<Animator>().SetBool ("Shadow_On",true);
				Enemy_stand.transform.position = new Vector3(0,0,1);
				Name_Object.GetComponent<Text> ().text = Player_Name;
			}else{
				Text = serif[2];
				Enemy_stand.GetComponent<Animator>().SetInteger("Face",face_Num);
				Enemy_stand.GetComponent<Animator>().SetBool ("Shadow_On",false);
				Enemy_stand.transform.position = new Vector3(0,0,0);
				Player_stand.GetComponent<Animator>().SetBool ("Shadow_On",true);
				Player_stand.transform.position = new Vector3(0,0,1);
				Name_Object.GetComponent<Text> ().text = Enemy_Name;
			}
		} else {
			EndTalk ();
		}
	}
	
	void EndTalk(){
		if(before){
			Instantiate (Call_Warning);
		}else{
			GameObject.Find ("Game_Pane(Clone)").GetComponent<Game_Pane>().EndGame();
		}
		Exit();
	}
	
	// Update is called once per frame
	public override void Overriden_Update () {
		Text_Object.GetComponent<Text> ().text = Text;
		Title_Object.GetComponent<Text> ().text = Title;
		if(Input.GetButtonDown ("Z")){
			now_Serif_Num ++;
			ReadSerif(now_Serif_Num);
		}
		if (Input.GetButtonDown ("X")) {
			EndTalk ();
		}
	}
}
