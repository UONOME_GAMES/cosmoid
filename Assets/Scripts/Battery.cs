using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Battery : Mover {
	//定数
	//撃ち方
	public const int ABSOLUTE = 0;
	public const int RELATIVE = 1;
	public const int ROCK_ON = 2;
	const int ABSOLUTE_DEGREE = 0;
	const int RELATIVE_DEGREE = 1;
	const int ROCK_ON_DEGREE = 2;
	const int ABSOLUTE_RANDOM = 3;
	const int RELATIVE_RANDOM = 4;
	const int ROCK_ON_RANDOM = 5;
	const int ROLLING = 6;

	//打ち出し方の設定
	private float[] shootValues = new float[5]; //自由に使える変数
	private int kind_of_shoot; //打ち出し方の種類
	private int ways = 1; //何ウェイ弾を打ち出すのか
	private float way_width = 0; //↑の角度幅
	private GameObject player; //プレイヤー(追尾用)
	private float _speed_shadow;
	private float speed_changer;
	private float _speed;

	public GameObject prefab;//打ち出す弾のプレハブ

	private Pool pool;//オブジェクトプール

	private SpriteRenderer SR;

	private AudioSource SE_Source;
	
	public AudioClip SE;

	[HideInInspector]
	public int battery_ID;

	public override void Initialize ()
	{
		base.Initialize ();
		SetShootDegree (RELATIVE, 0);
		SetWays (1, 0);
		SetOval (0, 0);
		position.transform.position = this.transform.position;
		_speed_shadow = 1;
		_speed = 0;
		speed_changer = 0;
	}

	public override void Start () {//Moverからオーバーライド
		base.Start ();
		SE_Source = GameObject.Find ("SE_Player").GetComponent<AudioSource> ();
		pool = transform.parent.GetComponent<Pool> ();
		SR = position.GetComponent<SpriteRenderer> ();
	}
	
//	public void SetLoop(float LoopTime,float IntervalTime,int bullets){
//		SetLoop (LoopTime, IntervalTime);
//		shoots = bullets;
//		if (loop_time == 0) {
//			shoot_timing = LoopTime / shoots;
//		}else{
//			shoot_timing = loop_time / shoots;
//		}
//	}

	public void SetWays(int Ways,float Way_W){
		ways = Ways;
		way_width = Way_W;
	}

	public void SetSpeed(float changer,float loops){
		_speed_shadow = changer - prefab.GetComponent<Bullet> ().speed;
		speed_changer = (changer - prefab.GetComponent<Bullet> ().speed) / ways * 2 * loops;
		_speed = 0;
	}
	
	Transform CalcShootDirection(Transform t){
		float val;
		switch (kind_of_shoot) {
		case ABSOLUTE_DEGREE:
			t.rotation = Quaternion.Euler(0,0,shootValues[0]);
			break;
		case RELATIVE_DEGREE:
			t.Rotate(new Vector3(0,0,shootValues[0]));
			break;
		case ROCK_ON_DEGREE:
			t.rotation = LookAt2D(player, t.gameObject);
			t.Rotate(new Vector3(0,0,shootValues[0]));
			break;
		case ABSOLUTE_RANDOM:
			val = Random.Range(0.0f,shootValues[0]);
			t.rotation = Quaternion.Euler(new Vector3(0,0,shootValues[1] - shootValues[0]/2 + val));
			break;
		case RELATIVE_RANDOM:
			val = Random.Range(0.0f,shootValues[0]);
			t.Rotate(new Vector3(0,0,shootValues[1] - shootValues[0]/2 + val));
			break;
		case ROCK_ON_RANDOM:
			val = Random.Range(0.0f,shootValues[0]);
			t.rotation = LookAt2D(player, t.gameObject);
			t.Rotate(new Vector3(0,0,shootValues[1] - shootValues[0]/2 + val));
			break;
		case ROLLING:
			t.rotation = Quaternion.Euler(new Vector3(0,0,shootValues[0] + shootValues[2] * 360));
			shootValues[2] = shootValues[2] + shootValues[1];
			//Debug.Log (shootValues[2]);
			break;
		}
		return t;
	}
	
	public void Shoot(){
		Bullet bullet;
		Bullet b =  prefab.GetComponent<Bullet>();
		if (ways - 1 > 0) {
			float way_interval = way_width / (ways - 1);
			Transform c_transform;
			for (int i = 0; i < ways; i++) {
				bullet = pool.GetBullet (prefab).GetComponent<Bullet> ();
				bullet.COLORS = b.COLORS;
				bullet.SCALES = b.SCALES;
				bullet.sprites = b.sprites;
				c_transform = CalcShootDirection(position.transform);
				c_transform.Rotate (new Vector3 (0, 0, way_interval * i- way_width / 2));
				//Debug.Log(c_transform.rotation.eulerAngles);
				_speed = Mathf.PingPong (speed_changer, _speed_shadow);
				bullet.shooted (c_transform, b.speed + _speed, b.move, b.reflection,battery_ID,b.lifeTime);
			}
			_speed = 0;
		} else {
			bullet = pool.GetBullet (prefab).GetComponent<Bullet> ();
			bullet.COLORS = b.COLORS;
			bullet.SCALES = b.SCALES;
			bullet.sprites = b.sprites;
			Transform t = CalcShootDirection(position.transform);
			position.transform.rotation = t.rotation;
			position.transform.position = t.position;
			bullet.shooted (t, b.speed + Mathf.PingPong(_speed, _speed_shadow), b.move, b.reflection,battery_ID,b.lifeTime);
		}
				//SE_Source.PlayOneShot(SE);
	}
	
	public override void Up_Working(){//Moverからオーバーライド
		if(!SR.enabled){
				SR.enabled = true;
			}
	}
	
	public override void Up_Not_Working(){//Moverからオーバーライド
		if(SR.enabled){
			SR.enabled = false;
		}
	}

	public void SetPosition(){
		if (homing != null) {
			this.transform.position = homing.transform.position;
			this.transform.rotation = homing.transform.rotation;
		}
	}

		//	public override void Up_Not_Interval(){//Moverからオーバーライド
//		base.Up_Not_Interval ();
//		if(loop_time == 0){
//				LoopOneshot();
//		}else{
//			if((Time.time - loop_rec) % shoot_timing <= Time.deltaTime){
//				_speed += speed_changer;
//				Shoot ();
//				SE_Source.PlayOneShot(SE);
//				//Debug.Log ("Shoot");
//			}
//		}
//		if(Time.time - loop_rec >= loop_time){
//			isInterval = true;
//			_speed = 0;
//			//Debug.Log ("Interval");
//		}
		//	}
	
	public void SetShootDegree(int kind,float degree){
		kind_of_shoot = kind;
		shootValues [0] = degree;
		player = transform.parent.gameObject.GetComponent<Plan> ().player;
	}
	
	public void SetShootRandom(int kind,float width,float degree){
		kind_of_shoot = kind + 3;
		shootValues [0] = width;
		shootValues [1] = degree;
		player = transform.parent.gameObject.GetComponent<Plan> ().player;
	}
	
		public void SetShootRolling(float Rolls,float startdeg,int bullets){
		kind_of_shoot = ROLLING;
		shootValues [0] = startdeg;
		shootValues [1] = Rolls / bullets;
		shootValues [2] = 0;
	}

		public void Play_SE(){
			SE_Source.PlayOneShot(SE);
		}
	
//	public void LoopOneshot(Mover mover){
//		bool beforework = mover.isWorking;
//		float beforeperce = mover.percentage;
//		mover.isWorking = true;
//		for(float i = 0;i < 1;i += shoot_timing * one_sec_perce){
//			mover.percentage = i;
//			mover.Move();
//			position.transform.position = mover.position.transform.position;
//			_speed += speed_changer;
//			Shoot ();
//		}
//		mover.percentage = beforeperce;
//		mover.isWorking = beforework;
//	}

//	public void LoopOneshot(){
//		//homingに従って1ループする
//		if (homing == null) {
//			LoopOneshot(this);
//		}else if(homing.GetComponent<Mover>() == null){
//			transform.position = homing.transform.position;
//			transform.rotation = homing.transform.rotation;
//			LoopOneshot(this);
//		}else{
//			LoopOneshot(homing.GetComponent<Mover>());
//		}
//		SE_Source.PlayOneShot (SE);
//	}

	public static Quaternion LookAt2D(GameObject target,GameObject host){
		Vector3 distance = target.transform.position - host.transform.position;
		float direction = Mathf.Atan2 (distance.y, distance.x) * Mathf.Rad2Deg;
		return Quaternion.AngleAxis (direction, Vector3.forward);
	}
}