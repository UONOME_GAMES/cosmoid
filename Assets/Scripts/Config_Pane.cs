﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Config_Pane : Transition
{
		public Slider BGM_Slider;
		public Slider SE_Slider;
		private AudioSource BGM_Source;
		private AudioSource SE_Source;
		public GameObject cursor;
		public GameObject[] Buttons = new GameObject[3];
		private int _nowButton = 0;
		private float _HorizonKey;
		public GameObject nextPane;

		public override void Start ()//スタート
		{
				base.Start ();
				BGM_Source = GameObject.Find ("BGM_Player").GetComponent<AudioSource> ();
				SE_Source = GameObject.Find ("SE_Player").GetComponent<AudioSource> ();
				BGM_Slider.value = BGM_Source.volume;
				SE_Slider.value = SE_Source.volume;
		}

		public override void Overriden_Update ()
		{
				BGM_Source.volume = BGM_Slider.value;
				SE_Source.volume = SE_Slider.value;
				if (Input.GetButtonDown ("Vertical") && Input.GetAxisRaw ("Vertical") > 0) {
						//Debug.Log ("up");
						_nowButton--;
				}
				if (Input.GetButtonDown ("Vertical") && Input.GetAxisRaw ("Vertical") < 0) {
						//Debug.Log ("down");
						_nowButton++;
				}
				_nowButton = Mathf.Clamp (_nowButton, 0, Buttons.Length - 1);
				//Debug.Log (_nowButton);
				cursor.transform.localPosition = Buttons [_nowButton].transform.localPosition;
				if (Input.GetButtonDown ("Z")) {
						if (_nowButton == 2) {
								Instantiate (nextPane);
								Exit ();
						}
				}
				_HorizonKey = Input.GetAxisRaw ("Horizontal");
				switch (_nowButton) {
				case 0://BGM
						BGM_Slider.value += _HorizonKey * 0.01f;
						break;
				case 1://SE
						SE_Slider.value += _HorizonKey * 0.01f;
						break;
				}
		}
}
