﻿using UnityEngine;
using System.Collections;

public class Continue_Pane : Transition {

	public GameObject[] Buttons;
	
	public GameObject[] Next_Panes;
	
	public GameObject cursor;

	public Sprite NotClear;
	
	private int _nowButton = 0;

	private bool _clear = true;

	private int _level = 1;

	private int _stage = 1;

	private int _android = 0;
	
	private int _weapon = 0;
	
	public override void Overriden_Update(){
		if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") > 0) {
			//Debug.Log ("up");
			_nowButton--;
		}
		if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") < 0) {
			//Debug.Log ("down");
			_nowButton++;
		}
		if(_clear){
			_nowButton = Mathf.Clamp (_nowButton, 0, Buttons.Length - 1);
		}else{
			_nowButton = Mathf.Clamp (_nowButton, 1, Buttons.Length - 1);
		}
		//Debug.Log (_nowButton);
		cursor.transform.localPosition = Buttons [_nowButton].transform.localPosition;
		if (Input.GetButtonDown("Z")) {
			GameObject obj = Instantiate(Next_Panes[_nowButton]) as GameObject;
			switch(_nowButton){
			case 0://nextstage
				obj.GetComponent<Game_Pane>().Initialize(_android,_weapon,_level,_stage + 1);
				break;
			case 1://retry
				obj.GetComponent<Game_Pane>().Initialize(_android,_weapon,_level,_stage);
				break;
			case 2://ranking

				break;
			}
			Exit();
		}
	}

	public void Setting(bool clear,int level,int stage,int android,int weapon){
		_level = level;
		_stage = stage;
		_android = android;
		_weapon = weapon;
		_clear = clear;
		if (!_clear) {
			transform.Find("Pane_CLEAR").GetComponent<SpriteRenderer>().sprite = NotClear;
			Buttons[0].GetComponent<SpriteRenderer>().enabled = false;
		}
	}
}
