﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(Animator))]
[RequireComponent (typeof(CircleCollider2D))]
public class Player : MonoBehaviour
{
		public int android;
		public float speed;
		public float reduceSpeed;
		public ParticleSystem _missile;
		public ParticleSystem _particle;
		public Animator cut_In;
		public float HP;
		[HideInInspector]
		public Slider _HPslider;
		[HideInInspector]
		public int weapon;
		private Animator _animator;
		private bool _leaping;
		[HideInInspector]
		public float AttackTime;
		private Text _Attacks;
		[HideInInspector]
		public GameObject enemy;
		[HideInInspector]
		public int _baseScore;
		private AudioSource SE_Source;
		public AudioClip Attack_SE;
		public AudioClip Damage_SE;
		// Use this for initialization
		void Awake ()
		{
				//transform.parent = GameObject.Find ("Game_Pane").transform;//type Debug
				transform.parent = GameObject.Find ("Game_Pane(Clone)").transform;
				gameObject.name = "Player";
				_animator = GetComponent<Animator> ();
				_HPslider = transform.parent.Find ("Canvas").Find ("Player_HP").GetComponent<Slider> ();
				_HPslider.maxValue = HP;
				_HPslider.minValue = 0;
				_HPslider.value = HP;
				_missile.transform.parent = this.transform.parent;
				_particle.transform.parent = this.transform.parent;
				cut_In.transform.parent = this.transform.parent;
				SE_Source = GameObject.Find ("SE_Player").GetComponent<AudioSource> ();
				_Attacks = transform.parent.Find("Canvas").Find("Player_Attacks").GetComponent<Text>();
		}
		// Update is called once per frame
		void Update ()
		{
				Move ();

				if (Input.GetButtonDown ("Z")) {
						if (enemy != null && enemy.GetComponent<Enemy> ()._nowPlan >= 1 && enemy.GetComponent<Enemy> ()._nowPlan <= enemy.GetComponent<Enemy> ().planMax) {
								if (!_particle.IsAlive ()) {
										Attack ();
								}
						}
				}

				if (_leaping) {
						LerpInitPosition ();
						if (Time.time - AttackTime > 0.5f) {
								_leaping = false;
						}
				}
		
				transform.position = Clamp ();
		}

		public void Damage ()
		{
				SE_Source.PlayOneShot (Damage_SE);
		}

		void Move ()
		{
				float currentSpeed = 1;
				// 右・左
				float x = Input.GetAxisRaw ("Horizontal");		
				// 上・下
				float y = Input.GetAxisRaw ("Vertical");
		
				// 移動する向きを求める
				Vector2 direction = new Vector2 (x, y).normalized;
		
				if (Input.GetButton ("Shift")) {
						currentSpeed = reduceSpeed;
				} else {
						currentSpeed = speed;
				}
		
				_animator.SetInteger ("direction", (int)(5 - (y * 3) + x));
		
				// 移動する向きとスピードを代入する
				GetComponent<Rigidbody2D>().velocity = direction * currentSpeed;

		}

		void Attack ()
		{
				_leaping = true;
				AttackTime = Time.time;
				//damage Calcration
				float distance = (12 - Vector3.Distance (enemy.transform.position, transform.position)) / 12;
				if (distance >= 0.85f) {
						cut_In.SetTrigger ("Excellent");
						_baseScore += 700;
				} else if (distance >= 0.65f) {
						cut_In.SetTrigger ("Great");
						_baseScore += 450;
				} else if (distance >= 0.4f) {
						cut_In.SetTrigger ("Good");
						_baseScore += 150;
				} else {
						cut_In.SetTrigger ("Bad");
						_baseScore -= 200;
				}
				float damage = 0;
				if (weapon == 0) {
						_missile.gameObject.transform.rotation = Quaternion.Euler (new Vector3 (-Battery.LookAt2D (enemy, this.gameObject).eulerAngles.z, 90, 0));
						//Debug.Log (Battery.LookAt2D(enemy,this.gameObject).eulerAngles);
						damage = (80 + android * 10) * distance * distance;
						_missile.transform.position = this.transform.position;
						_missile.Play ();
						_particle.gameObject.transform.position = new Vector3(0,4.8f,0);
				} else {
						damage = (100 + android * 10) * distance * (2 - distance);
						_particle.gameObject.transform.position = this.transform.position;
				}
				_baseScore += (int)(damage * 10);
				_particle.Play ();
				enemy.GetComponent<Enemy> ().Damage (damage);
				SE_Source.PlayOneShot (Attack_SE);
				_Attacks.text = "" + (int.Parse (_Attacks.text) + 1);
		}

		Vector2 Clamp ()
		{
				// 画面左下のワールド座標をビューポートから取得
				Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		
				// 画面右上のワールド座標をビューポートから取得
				Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));
		
				Vector2 pos = transform.position;
		
				// プレイヤーの位置が画面内に収まるように制限をかける
				pos.x = Mathf.Clamp (pos.x, min.x, max.x);
				pos.y = Mathf.Clamp (pos.y, min.y, max.y);
		
				return pos;
		}

		public void LerpInitPosition ()
		{
				_leaping = true;
				float x = Mathf.Lerp (transform.position.x, 0, 0.1f);
				float y = Mathf.Lerp (transform.position.y, -5, 0.1f);
				transform.position = new Vector2 (x, y);
		}
}
