﻿using UnityEngine;
using System.Collections;

public class Game_Pane : Transition
{
		public Player[] players;
		public Enemy[] enemys;
		public Color[] WEAPONCOLORS = new Color[2];
		public Talk_Pane[] beforeTalks = new Talk_Pane[24];
		public Talk_Pane[] afterTalks = new Talk_Pane[24];
		public GameObject result;
		[HideInInspector]
		public int _android;
		[HideInInspector]
		public int _weapon;
		[HideInInspector]
		public int _level;
		[HideInInspector]
		public int _stage;
		//[HideInInspector]
		public int _gameNum;
		private Player _player;
		private Enemy _enemy;

		public override void Start ()
		{
				base.Start ();
		}

		public override void Overriden_Update ()
		{
//		if (Input.GetKeyDown (KeyCode.Z)) {//Debug!----------------------
//			if(_enemy._nowPlan <= 0){
//				StartGame ();
//			}
//		}//--------------------------------------------------------------
				if (_enemy._nowPlan > 0 && _enemy._nowPlan <= _enemy.planMax) {

				}
		}

		public void StartGame ()
		{
				_player.enemy = _enemy.gameObject;
				_enemy._player = _player.gameObject;
				_player.LerpInitPosition ();
				_player.AttackTime = Time.time;
				_enemy.NextPlan ();
		}

		public void EndGame ()
		{
				GameObject r = Instantiate (result) as GameObject;
				bool clear = false;
				int T = 0;
				float P_HP = _player._HPslider.value / _player._HPslider.maxValue;
				int b_score = _player._baseScore;
				if (_enemy.FinishTime < _enemy.BonusTime) {
						T = (int)((_enemy.BonusTime - _enemy.FinishTime) * 700);
				}
				//calc Total
				int Total = (int)(b_score * P_HP) + T + (int)P_HP * 7000;
				if (Total >= _enemy.ClearScore) {
						clear = true;
				}
				int score = r.GetComponent<Result_Pane> ().StartResult (clear, b_score, P_HP, T);
				if (clear) {
						Cosmoid.SaveFrag (_level, _stage);
				}
				Cosmoid.SaveRank (_level, _stage, score, _player);
		}

		public void StartTalk (bool before, int LvStNo)
		{
				if (before) {
						Instantiate (beforeTalks [LvStNo].gameObject);
				} else {
						Instantiate (afterTalks [LvStNo].gameObject);
				}
		}

		public void Initialize (int android, int weapon, int level, int stage)
		{
				_android = android;
				_weapon = weapon;
				_level = level;
				_stage = stage;
				int[] pluses = new int[]{ 0, 3, 7 };
				_gameNum = pluses [_level - 1] + _stage - 1;
				GameObject obj;
				obj = Instantiate (players [_android].gameObject) as GameObject;
				_player = obj.GetComponent<Player> ();
				_player._particle.startColor = WEAPONCOLORS [_weapon];
				_player.weapon = _weapon;
				obj = Instantiate (enemys [_gameNum].gameObject) as GameObject;
				_enemy = obj.GetComponent<Enemy> ();
				StartTalk (true, _android * 12 + _gameNum);
		}
}
