using UnityEngine;
using System.Collections;

public class Plan_3_1 : Plan
{
		//----dictionary---
		//switch(enemy._nowPlan){
		//case :
		//
		//break;
		//}
		public override void Init ()
		{
				StopAllCoroutines ();
				base.Init ();
				switch (enemy._nowPlan) {
				case 1://Tactics_1
						battery [0].prefab = Bullets [0];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].SetLoop (1, 0);
						battery [0].SetWays (13, 270);
						break;
				case 2://Tactics_4
						battery [0].prefab = Bullets [1];
						battery [0].SetOval (3, 120);
						battery [0].SetShootRandom (Battery.ABSOLUTE, 45, -90);
						battery [0].SetLoop (1.5f, 0);
						battery [0].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DIM3);
						battery [0].transform.Rotate (new Vector3 (0, 0, 90));
						break;
				case 3://Tactics_15
						battery [0].prefab = Bullets [2];
						battery [0].SetLine (new Vector2 (-4, 6), new Vector2 (4, 6));
						battery [0].SetLoop (1, 0);
						battery [0].SetShootDegree (Battery.ABSOLUTE, -90);
						battery [0].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DIM3);
						battery [1].prefab = Bullets [11];
						battery [1].SetLine (new Vector2 (-4, 6), new Vector2 (3, 6));
						battery [1].SetLoop (3, 0);
						battery [1].SetShootDegree (Battery.ABSOLUTE, -90);
						battery [1].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DIM3);
						battery [2].prefab = Bullets [12];
						battery [2].SetLine (new Vector2 (-3, 6), new Vector2 (4, 6));
						battery [2].SetLoop (5, 0);
						battery [2].SetShootDegree (Battery.ABSOLUTE, -90);
						battery [2].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DIM3);
						break;
				case 4://Tactics_6
						battery [0].prefab = Bullets [4];
						battery [0].transform.position = new Vector2 (1.5f, 4.8f);
						battery [0].SetOval (2, 60);
						battery [0].SetLoop (3, 0);
						battery [0].SetShootRolling (0.5f, 0.5f, 12);
						battery [0].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DEFAULT);
						battery [1].prefab = Bullets [4];
						battery [1].transform.position = new Vector2 (-1.5f, 4.8f);
						battery [1].transform.Rotate (new Vector3 (0, 0, 180));
						battery [1].SetOval (2, 60);
						battery [1].SetLoop (3, 0);
						battery [1].SetShootRolling (-0.5f, -0.5f, 12);
						battery [1].StartLoop (Mover.GO_BACK_MINUS, -1, Mover.DEFAULT);
						battery [2].prefab = Bullets [3];
						battery [2].SetShootDegree (Battery.ROCK_ON, 0);
						battery [2].SetWays (9, 120);
						battery [2].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 5://Tactics_7
						battery [0].isWorking = true;
						battery [0].prefab = Bullets [5];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [1].prefab = Bullets [6];
						battery [1].SetOval (0, 27);
						battery [1].SetWays (13, 332);
						battery [1].transform.Rotate (new Vector3(0,0,180));
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						break;
				case 6://Tactics_5
						battery [0].prefab = Bullets [7];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [1].prefab = Bullets [8];
						battery [1].SetOval (3, 90);
						battery [1].SetShootRandom (Battery.ABSOLUTE, 60, -90);
						battery [1].SetLoop (1.5f, 0);
						battery [1].transform.Rotate (new Vector3 (0, 0, 90));
						battery [1].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DIM3);
						battery [2].prefab = Bullets [9];
						battery [2].SetShootDegree (Battery.ROCK_ON, 0);
						battery [2].SetWays (5, 15);
						break;
				case 7://Tactics_31
						battery [0].prefab = Bullets [10];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].SetWays (8, 270);
						break;
				}
		}

		public override void Update ()
		{
				switch (enemy._nowPlan) {
				case 1:
						if (CalcTiming (0.7f, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan1");
						}
						break;
				case 2:
						if (CalcTiming (0.05f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						break;
				case 3:
						if (CalcTiming (0.05f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						if (CalcTiming (0.2f, 0) <= Time.deltaTime) {
								battery [1].Shoot ();
								battery [1].Play_SE ();
						}
						if (CalcTiming (0.3f, 0) <= Time.deltaTime) {
								battery [2].Shoot ();
								battery [2].Play_SE ();
						}
						break;
				case 4:
						if (CalcTiming (2, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan4");
						}
						if (CalcTiming (0.08f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [1].Shoot ();
								battery [1].Play_SE ();
						}
						break;
				case 5:
						if (CalcTiming (2.2f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						if (CalcTiming (2.2f, 0.1f) <= Time.deltaTime) {
								battery [1].homing = pool.GetBulletsByCountOver (0, 1, 0) [0];
								StartCoroutine ("Plan5");
								//battery [1].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
						}
						if (nowLoop - Time.time > 2.2f) {
								nowLoop = Time.time;
						}
						break;
				case 6:
						if (CalcTiming (1.3f, 0.5f) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						if (CalcTiming (0.125f, 0) <= Time.deltaTime) {
								battery [1].Shoot ();
								battery [1].Play_SE ();
						}
						break;
				case 7:
						if (CalcTiming (1, 0.3f) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						if (CalcTiming (3, 2f) <= Time.deltaTime) {
								foreach(GameObject bul in pool.GetBulletsByCountOver(0,3,0)){
										bul.transform.rotation = Battery.LookAt2D (player, bul);
										bul.GetComponent<Bullet> ().burst (bul.transform, 2.4f);
								}
						}
						break;
				}
		}

		IEnumerator Plan1(){
				battery [0].prefab.GetComponent<Bullet> ().speed = 2.8f;
				battery [0].Shoot ();
				battery [0].Play_SE ();
				yield return new WaitForSeconds (0.05f);
				battery [0].prefab.GetComponent<Bullet> ().speed = 3.3f;
				battery [0].Shoot ();
				battery [0].Play_SE ();
				yield return new WaitForSeconds (0.05f);
				battery [0].prefab.GetComponent<Bullet> ().speed = 4;
				battery [0].Shoot ();
				battery [0].Play_SE ();
				yield break;
		}

		IEnumerator Plan4 ()
		{
				for (int i = 0; i < 3; i++) {
						yield return new WaitForSeconds (0.33f);
						battery [2].Shoot ();
						battery [2].Play_SE ();
				}
				yield break;
		}

		IEnumerator Plan5 ()
		{
				for (int i = 0; i < 6; i++) {
						yield return new WaitForSeconds (0.2f);
						battery [1].SetPosition ();
						battery [1].Shoot ();
						battery [1].Play_SE ();
				}
				yield break;
		}

		public override void TriggerOnWall (GameObject obj)
		{
				switch (enemy._nowPlan) {
				case 6://Tactics_25
						if (obj.GetComponent<Bullet> ().battery_ID == 0) {
								battery [2].transform.position = obj.transform.position;
								battery [2].Shoot ();
								battery [2].Play_SE ();
								obj.GetComponent<Animator> ().SetBool ("Created", false);
						}
						break;
				}
		}

		public override void RaytoWallCollision (Vector2 c_position)
		{

		}
}
