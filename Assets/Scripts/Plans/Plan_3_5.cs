using UnityEngine;
using System.Collections;

public class Plan_3_5 : Plan
{
		//----dictionary---
		//switch(enemy._nowPlan){
		//case :
		//
		//break;
		//}
		int value;

		public override void Init ()
		{
				StopAllCoroutines ();
				base.Init ();
				switch (enemy._nowPlan) {
				case 1://Tactics_16
						mover [0].SetLoop (0.6f, 0);//y
						mover [1].SetLoop (0.4f, 0);//x
						battery [0].prefab = Bullets [0];
						battery [1].prefab = Bullets [0];
						battery [0].SetWays (2, 180);
						battery [1].SetWays (2, 180);
						battery [0].SetShootDegree (Battery.ABSOLUTE, 90);
						battery [1].SetShootDegree (Battery.ABSOLUTE, 0);
						battery [0].homing = mover [0].position;
						battery [1].homing = mover [1].position;
						break;
				case 2://Tactics_17
						battery [0].prefab = Bullets [2];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						for (int i = 0; i < 3; i++) {
								battery [i + 1].prefab = Bullets [3];
								battery [i + 1].SetShootRolling (1.5f, i * 60, 40);
								battery [i + 1].SetWays (2, 180);
								battery [i + 1].SetLoop (0.7f, 0);
						}
						value = 1;
						//battery[0].StartLoop(Mover.PLUS,-1,Mover.DEFAULT);
						break;
				case 3://Tactics_22
						battery [0].SetLine (new Vector2 (-4, -6), new Vector2 (4, 6));
						battery [1].SetLine (new Vector2 (4, 6), new Vector2 (4, -6));
						battery [2].SetLine (new Vector2 (4, -6), new Vector2 (-4, 6));
						battery [3].SetLine (new Vector2 (-4, 6), new Vector2 (-4, -6));
						for (int i = 0; i < 4; i++) {
								battery [i].SetLoop (1, 0);
								battery [i].prefab = Bullets [4];
								battery [i].SetShootRolling (0.5f, i * 45, 26);
								battery [i].SetWays (2, 180);
						}
						battery [4].prefab = Bullets [19];
						battery [4].SetShootDegree (Battery.ROCK_ON, 0);
						break;
				case 4://Tactics_23
						for (int i = 0; i < 3; i++) {
								laser [i].SetLaser (0.6f, 1, 10);
						}
						battery [0].prefab = Bullets [5];
						battery [0].SetLine (new Vector2 (5, 6), new Vector2 (5, -6));
						battery [0].SetLoop (1, 0);
						battery [0].SetShootRandom (Battery.ABSOLUTE, 15, 180);
						battery [0].StartLoop (Mover.PLUS, Mover.INFINITY, Mover.DEFAULT);
						battery [1].prefab = Bullets [5];
						battery [1].SetLine (new Vector2 (-5, 6), new Vector2 (-5, -6));
						battery [1].SetLoop (1, 0);
						battery [1].SetShootRandom (Battery.ABSOLUTE, 15, 0);
						battery [1].StartLoop (Mover.PLUS, Mover.INFINITY, Mover.DEFAULT);
						break;
				case 5://Tactics_24
						battery [0].prefab = Bullets [7];
						battery [1].prefab = Bullets [8];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						break;
				case 6://Tactics_26
						for (int i = 0; i < 3; i++) {
								battery [i].prefab = Bullets [9];
								battery [i].SetShootDegree (Battery.ROCK_ON, 0);
								battery [i].SetLoop (1, 0);
								battery [i + 3].prefab = Bullets [10];
								battery [i + 3].SetLoop (1, 0);
								//battery [i + 3].SetWays (6, 360);
								//battery [i + 3].SetShootRolling (0.5f, 0, 36);
						}
						battery [0].StartLoop (Mover.PLUS, Mover.INFINITY, Mover.DEFAULT);
						break;
				case 7://Tactics_29
						battery [0].prefab = Bullets [11];
						battery [0].SetLissajous (3, 2, 3, 1);
						battery [0].SetLoop (5f, 0);
						battery [0].SetShootRolling (1f, 0, 160);
						battery [0].SetWays (3, 240);
						battery [1].prefab = Bullets [15];
						battery [1].SetOval (0.2f, 330);
						break;
				case 8://Tactics_34
						mover [0].SetLissajous (1, 2, 3, Mathf.PI / 4);
						mover [0].SetLoop (5.6f, 0);
						battery [0].prefab = Bullets [12];
						battery [1].prefab = Bullets [13];
						battery [0].homing = mover [0].position;
						battery [1].homing = mover [0].position;
						battery [0].SetOval (0, 5);
						battery [0].SetLoop (1, 0);
						battery [0].SetShootRandom (Battery.ABSOLUTE, 30, -90);
						battery [1].SetWays (4, 270);
						battery [1].SetLoop (1, 0);
						battery [1].SetShootRolling (-2f, -90, 3);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [1].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 9://Tactics_35
						battery [0].prefab = Bullets [14];
						battery [1].prefab = Bullets [15];
						battery [0].SetOval (0, 360);
						battery [1].SetOval (1, 360);
						battery [0].SetLoop (0.7f, 0);
						battery [0].SetShootRolling (1, -90, 11);
						battery [1].SetLoop (0.5f, 0);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [1].StartLoop (Mover.MINUS, -1, Mover.DEFAULT);
						battery [2].prefab = Bullets [16];
						battery [2].SetOval (0.5f, 360);
						battery [2].SetLoop (1, 0);
						break;
				case 10://Tactics_38
						battery [0].prefab = Bullets [17];
						battery [0].SetLoop (1, 0);
						battery [0].SetOval (0, 360);
						battery [0].SetShootRandom (Battery.RELATIVE, 60, 0);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [1].prefab = Bullets [18];
						battery [1].SetWays (5, 360 - 72);
						battery [1].SetLoop (0.1f, 0);
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						battery [2].prefab = Bullets [20];
						battery [2].SetOval (0, 360);
						battery [2].SetWays (5, 30);
						battery [2].SetLoop (1, 0);
						battery [2].SetShootRolling (1, 0, 60);
						battery [2].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						value = 0;
						break;
				}
		}

		IEnumerator Plan1 ()
		{
				mover [0].SetLine (new Vector2 (player.transform.position.x, 6), new Vector2 (player.transform.position.x, -6));
				mover [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				battery [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				for (int i = 0; i < 20; i++) {
						battery [0].Shoot ();
						yield return new WaitForSeconds (0.02f);
				}
				foreach (GameObject bullet in pool.GetBulletsByCountOver(0.01f,0)) {
						bullet.GetComponent<Bullet> ().move = true;
						bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
				}
				yield return new WaitForSeconds (0.6f);
				mover [1].SetLine (new Vector2 (-4, player.transform.position.y), new Vector2 (4, player.transform.position.y));
				mover [1].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				battery [1].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				for (int i = 0; i < 25; i++) {
						battery [1].Shoot ();
						yield return new WaitForSeconds (0.02f);
				}
				foreach (GameObject bullet in pool.GetBulletsByCountOver(0.01f,1)) {
						bullet.GetComponent<Bullet> ().move = true;
						bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
				}
				yield return new WaitForSeconds (0.6f);
				mover [0].SetLine (new Vector2 (player.transform.position.x, -6), new Vector2 (player.transform.position.x, 6));
				mover [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				battery [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				for (int i = 0; i < 20; i++) {
						battery [0].Shoot ();
						yield return new WaitForSeconds (0.02f);
				}
				foreach (GameObject bullet in pool.GetBulletsByCountOver(0.01f,0)) {
						bullet.GetComponent<Bullet> ().move = true;
						bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
				}
				yield return new WaitForSeconds (0.6f);
				mover [1].SetLine (new Vector2 (4, player.transform.position.y), new Vector2 (-4, player.transform.position.y));
				mover [1].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				battery [1].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				for (int i = 0; i < 25; i++) {
						battery [1].Shoot ();
						yield return new WaitForSeconds (0.02f);
				}
				foreach (GameObject bullet in pool.GetBulletsByCountOver(0.01f,1)) {
						bullet.GetComponent<Bullet> ().move = true;
						bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
				}
		}

		IEnumerator Plan2 ()
		{
				battery [0].percentage = 0;
				for (int i = 0; i < 3; i++) {
						battery [0].Shoot ();
						yield return new WaitForSeconds (0.8f);
				}
		}

		IEnumerator Plan2_1 ()
		{
				battery [1].percentage = 0;
				battery [1].StartLoop (Mover.PLUS, 1, Mover.DIM2_EASE_IN);
				for (int i = 0; i < 38; i++) {
						battery [1].Shoot ();
						yield return new WaitForSeconds (0.01f);
				}
		}

		IEnumerator Plan2_2 ()
		{
				battery [2].percentage = 0;
				battery [2].StartLoop (Mover.PLUS, 1, Mover.DIM2_EASE_IN);
				for (int i = 0; i < 38; i++) {
						battery [2].Shoot ();
						yield return new WaitForSeconds (0.01f);
				}
		}

		IEnumerator Plan2_3 ()
		{
				battery [3].percentage = 0;
				battery [3].StartLoop (Mover.PLUS, 1, Mover.DIM2_EASE_IN);
				for (int i = 0; i < 38; i++) {
						battery [3].Shoot ();
						yield return new WaitForSeconds (0.01f);
				}
		}

		IEnumerator Plan3 ()
		{
				for (int i = 0; i < 4; i++) {
						battery [i].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
						for (int j = 0; j < 25; j++) {
								yield return new WaitForSeconds (0.04f);
								battery [i].Shoot ();
								battery [i].Play_SE ();
						}
						battery [4].transform.position = battery [i].position.transform.position;
						battery [4].Shoot ();
						battery [4].Play_SE ();
				}
		}

		IEnumerator Plan5 ()
		{
				for (int i = 8; i > 0; i--) {
						battery [0].SetWays (2, i * 12);
						for (int j = 0; j < 5; j++) {
								battery [0].prefab.GetComponent<Bullet> ().speed = j * 0.8f;
								battery [0].Shoot ();
						}
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.1f);
				}
				//for (int i = 0; i < 20; i++) {
				yield return new WaitForSeconds (0.05f);
				battery [1].Shoot ();
				battery [1].Play_SE ();
				//}
				yield break;
		}

		IEnumerator Plan7 ()
		{
				for (int i = 0; i < 80; i++) {
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.0625f);
				}
		}

		IEnumerator Plan8 ()
		{
				for (int j = 0; j < 14; j++) {
						for (int i = 0; i < 8; i++) {
								battery [0].percentage = 0.125f * i;
								battery [0].CalcPos ();
								battery [0].Shoot ();
						}
						for (int i = 0; i < 3; i++) {
								battery [1].percentage = 0.333f * i;
								battery [1].CalcPos ();
								battery [1].Shoot ();
						}
						yield return new WaitForSeconds (0.2f);
						for (int i = 0; i < 8; i++) {
								battery [0].percentage = 0.125f * i;
								battery [0].CalcPos ();
								battery [0].Shoot ();
						}
						yield return new WaitForSeconds (0.2f);
						foreach (GameObject bullet in pool.GetBulletsByCountOver(0.3f,0)) {
								bullet.GetComponent<Bullet> ().move = true;
								bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
						}
				}
		}

		IEnumerator Plan10 ()
		{
				battery [1].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				for (int i = 0; i < 3; i++) {
						battery [1].Shoot ();
						battery [1].Play_SE ();
						yield return new WaitForSeconds (0.04f);
				}
		}

		public override void Update ()
		{
				switch (enemy._nowPlan) {
				case 1:
						if (CalcTiming (6, 0.5f) <= Time.deltaTime) {
								StartCoroutine ("Plan1");

						}
						break;
				case 2:
						if (CalcTiming (3.2f, 0.5f) <= Time.deltaTime) {
								StartCoroutine ("Plan2");
						}
						break;
				case 3:
						if (CalcTiming (4.2f, 0.1f) <= Time.deltaTime) {
								StartCoroutine ("Plan3");
						}
						break;
				case 4:
						if (CalcTiming (1.7f, 0) <= Time.deltaTime) {
								for (int i = 0; i < 25; i++) {
										battery [0].percentage = 0.04f * i;
										battery [0].CalcPos ();
										battery [0].Shoot ();
										battery [1].percentage = 0.04f * i;
										battery [1].CalcPos ();
										battery [1].Shoot ();
								}
								battery [0].Play_SE ();
						}
						for (int i = 0; i < 3; i++) {
								if (CalcTiming (3, i * 0.3f) <= Time.deltaTime) {
										laser [i].transform.position = new Vector2 (player.transform.position.x, 6);
										laser [i].transform.rotation = Quaternion.Euler (0, 0, -90);
										laser [i].ShootLaser ();
								}
						}
						break;
				case 5:
						if (CalcTiming (2.5f, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan5");
						}
						break;
				case 6:
						for (int i = 0; i < 3; i++) {
								if (CalcTiming (6, i * 2 + 0.5f) <= Time.deltaTime) {
										battery [i].Shoot ();
										battery [i].Play_SE ();
								}
								if (CalcTiming (0.16f, 0) <= Time.deltaTime) {
										if (battery [i + 3].isWorking) {
												battery [i + 3].Shoot ();
												battery [i + 3].Play_SE ();
										}
								}
								if (CalcTiming (6, i * 2 + 0.6f) <= Time.deltaTime) {
										if (pool.GetBulletsByCountOver (0, 1, i).Length >= 1) {
												battery [i + 3].homing = pool.GetBulletsByCountOver (0, 1, i) [0];
												battery [i + 3].StartLoop (Mover.PLUS, 4, Mover.DEFAULT);
										}
								}
						}
						break;
				case 7:
						if (CalcTiming (5.4f, 0.1f) <= Time.deltaTime) {
								battery [0].percentage = 0;
								battery [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
								StartCoroutine ("Plan7");
						}
						if (CalcTiming (2,0.1f) <= Time.deltaTime) {
								battery [1].SetShootDegree (Battery.RELATIVE, 60);
								for (int i = 0; i < 12; i++) {
										battery [1].percentage = 0.083f * i;
										battery [1].CalcPos ();
										battery [1].Shoot ();
								}
								battery [1].SetShootDegree (Battery.RELATIVE, -60);
								for (int i = 0; i < 12; i++) {
										battery [1].percentage = 0.083f * i;
										battery [1].CalcPos ();
										battery [1].Shoot ();
								}
						}
						break;
				case 8:
						if (CalcTiming (6.6f, 0.5f) <= Time.deltaTime) {
								mover [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
								StartCoroutine ("Plan8");
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(1,1)) {
								bullet.GetComponent<Bullet> ().move = true;
								bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
						}
						break;
				case 9:
						if (CalcTiming (0.0625f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
						}
						if (CalcTiming (0.08f, 0) <= Time.deltaTime) {
								battery [1].Shoot ();
						}
						if (CalcTiming (2, 0) <= Time.deltaTime) {
								battery [2].transform.rotation = Battery.LookAt2D (player, battery [2].gameObject);
								battery [2].SetShootDegree (Battery.RELATIVE, 200);
								for (int i = 0; i < 16; i++) {
										battery [2].percentage = 0.0625f * i;
										battery [2].CalcPos ();
										battery [2].Shoot ();
								}
								battery [2].SetShootDegree (Battery.RELATIVE, -200);
								for (int i = 0; i < 16; i++) {
										battery [2].percentage = 0.0625f * i;
										battery [2].CalcPos ();
										battery [2].Shoot ();
								}
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(2.5f,0)) {
								bullet.GetComponent<Bullet> ().move = false;
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(2.5f,1)) {
								bullet.GetComponent<Bullet> ().move = false;
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(3.3f,0)) {
								bullet.GetComponent<Bullet> ().move = true;
								bullet.transform.Rotate (new Vector3 (0, 0, Random.Range (0, 360)));
								bullet.GetComponent<Bullet> ().burst (bullet.transform, 1);
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(3.3f,1)) {
								bullet.GetComponent<Bullet> ().move = true;
								bullet.transform.Rotate (new Vector3 (0, 0, Random.Range (0, 360)));
								bullet.GetComponent<Bullet> ().burst (bullet.transform, 1);
						}
						break;
				case 10:
						if (CalcTiming (0.05f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						if (CalcTiming (0.4f, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan10");
						}
						if (CalcTiming (0.1f, 0) <= Time.deltaTime) {
								battery [2].Shoot ();
								battery [2].Play_SE ();
						}
						if (CalcTiming (2.5f, 1) <= Time.deltaTime) {
								laser [value].SetLaser (0.6f, 3, 10 + Random.Range (0, 5));
								laser [value].transform.Rotate (new Vector3 (0, 0, Random.Range (0, 360)));
								laser [value].ShootLaser ();
								value++;
								if (value > 2) {
										value = 0;
								}
								battery [1].StartLoop (Mover.PLUS, 5, Mover.DEFAULT);
						}
						break;
				}
		}

		public override void TriggerOnWall (GameObject obj)
		{
				switch (enemy._nowPlan) {
				case 2:
						if (obj.GetComponent<Bullet> ().battery_ID == 0) {
								battery [value].SetLine (obj.transform.position, transform.position);
								switch (value) {
								case 1:
										StartCoroutine ("Plan2_1");
										break;
								case 2:
										StartCoroutine ("Plan2_2");
										break;
								case 3:
										StartCoroutine ("Plan2_3");
										break;
								}
								obj.GetComponent<Animator> ().SetBool ("Created", false);
								value++;
								if (value > 3) {
										value = 1;
								}
						}
						break;
				}
		}

		public override void Invoked_Method ()
		{
				switch (enemy._nowPlan) {
				case 7:

						break;
				}
		}

		public override void RaytoWallCollision (Vector2 c_position)
		{

		}
}
