using UnityEngine;
using System.Collections;

public class Plan_1_3 : Plan
{
		//	----dictionary---
		//	switch(enemy._nowPlan){
		//	case :
		//
		//	break;
		//	}
		//	battery[0].prefab = Bullets[0];
		//	battery[0].SetShootDegree (Battery.ROCK_ON,0);
		//	battery[0].SetLoop(2,2,1);
		//	battery[0].StartLoop(Mover.PLUS,-1,Mover.DEFAULT);
		public override void Init ()
		{
				StopAllCoroutines ();
				base.Init ();
				switch (enemy._nowPlan) {
				case 1://Tactics_2
						battery [0].prefab = Bullets [0];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].SetWays (9, 332);
						battery [1].prefab = Bullets [1];
						battery [1].transform.localPosition = new Vector2 (2, -1);
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						battery [1].SetWays (10, 330);
						battery [2].prefab = Bullets [1];
						battery [2].transform.localPosition = new Vector2 (-2, -1);
						battery [2].SetShootDegree (Battery.ROCK_ON, 0);
						battery [2].SetWays (10, 330);
						break;
				case 2://Tactics_10
						battery [0].transform.localPosition = new Vector2 (0, 0);
						battery [0].prefab = Bullets [2];
						battery [0].SetShootDegree (Battery.ROCK_ON, 180);
						battery [0].SetWays (24, 330);
						battery [1].prefab = Bullets [3];
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						//battery [2].prefab = Bullets [4];
						//battery [2].SetShootRandom (Battery.ROCK_ON, 60, 180);
						battery [1].transform.position = new Vector2 (0, 0);
						//battery [2].transform.position = new Vector2 (0, 0);
						battery [1].isWorking = true;
						break;
				case 3://Tactics_32
						battery [1].prefab = Bullets [5];
						battery [1].SetShootRandom (Battery.ABSOLUTE, 100, 90);
						battery [1].SetWays (2, 180);
						battery [1].SetLoop (1.6f, 1.2f);
						battery [1].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [0].prefab = Bullets [6];
						battery [0].SetShootRandom (Battery.ABSOLUTE, 100, 90);
						battery [0].SetWays (2, 180);
						battery [0].SetLoop (2, 2);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 4://Tactics_30
						for (int i = 0; i < 6; i++) {
								laser [i].SetLaser (1, 0.8f, 8, 0.8f);
								laser [i].transform.Rotate (new Vector3 (0, 0, i * 60 + 10));
								laser [i].RollSpeed = 16;
								laser [i].ShootLaser ();
						}
						battery [0].prefab = Bullets [7];
						battery [0].SetOval (0.8f, 360);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 5://Tactics_40
						laser [0].SetLaser (0.8f, 1, 10);
						battery [0].prefab = Bullets [8];
						battery [0].SetLoop (1.5f, 0);
						battery [0].SetShootRolling (3, 0, 30);
						break;
				case 6://Tactics_33
						laser [0].SetLaser (0.6f, 0.8f, 10);
						laser [1].SetLaser (0.6f, 0.8f, 10);
						battery [0].prefab = Bullets [9];
						battery [0].transform.position = this.transform.position;
						battery [0].transform.Rotate (0, 0, 180);
						battery [0].SetOval (10, 360);
						battery [0].SetShootDegree (Battery.RELATIVE, 180);
						battery [0].SetLoop (1.6f, 0);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				}
		}

		IEnumerator Plan1 ()
		{
				yield return new WaitForSeconds (1);
				battery [1].Shoot ();
				battery [2].Shoot ();
				battery [1].Play_SE ();
		}

		IEnumerator Plan2 ()
		{
//				battery [1].transform.position = new Vector2 (0, 0);
//				battery [2].transform.position = new Vector2 (0, 0);
				battery [1].Shoot ();
				for (int i = 0; i < 5; i++) {
						battery [0].prefab.GetComponent<Bullet> ().speed = 2 + i * 0.4f;
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.1f);
				}
				yield break;
		}

		IEnumerator Plan3 ()
		{
				for (int i = 0; i < 4; i++) {
						battery [1].Shoot ();
						battery [1].Play_SE ();
						yield return new WaitForSeconds (0.1f);
				}
				yield break;
		}

		IEnumerator Plan4 ()
		{
				for (int j = 0; j < 16; j++) {
								battery [0].percentage = 0.0625f * j;
								battery [0].CalcPos ();
								battery [0].Shoot ();
				}
				yield break;
		}

		IEnumerator Plan5 ()
		{
				for (int i = 0; i < 20; i++) {
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.05f);
				}
				yield break;
		}

		public override void Update ()
		{
				switch (enemy._nowPlan) {
				case 1:
						if (CalcTiming (2, 0.5f) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
								StartCoroutine ("Plan1");
						}
						break;
				case 2:
						battery [1].transform.position = new Vector2 (0, 0);
						battery [2].transform.position = new Vector2 (0, 0);
						if (CalcTiming (1.3f, 0.1f) <= Time.deltaTime) {
								StartCoroutine ("Plan2");
						}
//						if (CalcTiming (0.4f, 0.1f) <= Time.deltaTime) {
//								battery [2].Shoot ();
//								battery [2].Play_SE ();
//						}
						break;
				case 3:
						if (CalcTiming (1.6f, 0.5f) <= Time.deltaTime) {
								StartCoroutine ("Plan3");
						}
						if (CalcTiming (2f, 0.5f) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						break;
				case 4:
						if (CalcTiming (1.2f, 0.5f) <= Time.deltaTime) {
								StartCoroutine ("Plan4");
						}
						break;
				case 5:
						if (CalcTiming (1.8f, 0.5f) <= Time.deltaTime) {
								laser [0].transform.rotation = Battery.LookAt2D (player, laser [0].gameObject);
								laser [0].ShootLaser ();
								battery [0].SetLine (this.transform.position, player.transform.position);
								battery [0].percentage = 0;
								battery [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
								StartCoroutine ("Plan5");
						}
						break;
				case 6:
						if (CalcTiming (2.5f, 0.5f) <= Time.deltaTime) {
								laser [0].transform.rotation = Battery.LookAt2D (player, laser [0].gameObject);
								laser [1].transform.rotation = Battery.LookAt2D (player, laser [1].gameObject);
								laser [0].transform.Rotate (new Vector3 (0, 0, 15));
								laser [1].transform.Rotate (new Vector3 (0, 0, -15));
								laser [0].ShootLaser ();
								laser [1].ShootLaser ();
						}
						if (CalcTiming (0.08f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						break;
				}
		}

		public override void RaytoWallCollision (Vector2 c_position)
		{

		}
}
