using UnityEngine;
using System.Collections;

public class Plan_3_4 : Plan
{
		//----dictionary---
		//switch(enemy._nowPlan){
		//case :
		//
		//break;
		//}
		public override void Init ()
		{
				StopAllCoroutines ();
				base.Init ();
				switch (enemy._nowPlan) {
				case 1://Tactics_13
						mover [0].SetOval (3f, 360);
						mover [0].SetLoop (1.7f, 0);
						mover [0].StartLoop (Mover.PLUS, Mover.INFINITY, Mover.DEFAULT);
						battery [0].prefab = Bullets [0];
						battery [0].homing = mover [0].position;
						battery [0].SetLoop (0.14f, 0);
						battery [0].StartLoop (Mover.PLUS, Mover.INFINITY, Mover.DEFAULT);
						mover [1].SetOval (2f, 360);
						mover [1].SetLoop (1.2f, 0);
						mover [1].StartLoop (Mover.MINUS, Mover.INFINITY, Mover.DEFAULT);
						battery [1].prefab = Bullets [13];
						battery [1].homing = mover [1].position;
						battery [1].SetLoop (0.14f, 0);
						battery [1].StartLoop (Mover.PLUS, Mover.INFINITY, Mover.DEFAULT);
						break;
				case 2://Tactics_14
						battery [0].prefab = Bullets [1];
						battery [0].SetLeaf (1, 6, 1, 1);
						battery [0].SetWays (2, 30);
						battery [0].SetLoop (1f, 0);
						battery [0].SetShootDegree (Battery.RELATIVE, 45);
						battery [2].prefab = Bullets [1];
						battery [2].SetLeaf (1, 6, 1, 1);
						battery [2].SetLoop (1f, 0);
						battery [2].SetShootDegree (Battery.RELATIVE, -45);
						battery [1].prefab = Bullets [2];
						battery [1].SetOval (0, 360);
						battery [1].SetLoop (1, 0);
						battery [1].SetShootDegree (Battery.ABSOLUTE, -90);
						battery [1].SetWays (7, 120);
						break;
				case 3://Tactics_18
						battery [0].prefab = Bullets [3];
						battery [0].SetLissajous (1.5f, 3, 2, 0);
						battery [0].SetLoop (3, 0);
						battery [0].SetShootRandom (Battery.ROCK_ON, 15, 0);
						battery [0].StartLoop (Mover.PLUS, Mover.INFINITY, Mover.DEFAULT);
						battery [1].prefab = Bullets [4];
						battery [1].SetLissajous (1.8f, 2, 1, 5 / 8 * Mathf.PI);
						battery [1].SetLoop (3f, 0);
						battery [1].StartLoop (Mover.PLUS, Mover.INFINITY, Mover.DEFAULT);
						break;
				case 4://Tactics_20
						battery [0].prefab = Bullets [5];
						battery [0].transform.Rotate (new Vector3 (0, 0, 110));
						battery [0].SetHypocycloid (1.7f, 5);
						battery [0].SetShootDegree (Battery.RELATIVE, 45);
						battery [0].SetLoop (1, 0);
						battery [1].prefab = Bullets [6];
						battery [1].SetOval (0.8f,360);
						battery [1].SetLoop (0.96f, 0);
						break;
				case 5://Tactics_21
						battery [0].prefab = Bullets [7];
						battery [0].SetOval (4.5f, 270);
						battery [0].transform.Rotate (new Vector3 (0, 0, 90));
						battery [0].transform.localPosition = new Vector2 (0, -1);
						battery [0].SetShootDegree (Battery.RELATIVE, 170);
						battery [0].SetLoop (3, 0);
						battery [0].StartLoop (Mover.GO_BACK_PLUS, Mover.INFINITY, Mover.DEFAULT);
						battery [1].prefab = Bullets [8];
						battery [1].SetOval (3.8f, 270);
						battery [1].transform.Rotate (new Vector3 (0, 0, 90));
						battery [1].transform.localPosition = new Vector2 (0, -1);
						battery [1].SetShootDegree (Battery.RELATIVE, -175);
						battery [1].SetLoop (2.1f, 0);
						battery [1].StartLoop (Mover.GO_BACK_MINUS, Mover.INFINITY, Mover.DEFAULT);
						break;
				case 6://Tactics_25
						battery [0].prefab = Bullets [9];
						battery [0].SetShootRandom (Battery.RELATIVE, 360, 0);
						battery [0].SetLoop (1, 0);
						battery [0].StartLoop (Mover.PLUS, Mover.INFINITY, Mover.DEFAULT);
						battery [1].prefab = Bullets [10];
						battery [1].SetWays (3, 9);
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						battery [1].SetLoop (1, 0);
						break;
				case 7://Tactics_27
						for (int i = 0; i < 8; i++) {
								laser [i].SetLaser (0.8f, 0.6f, 8, 0.3f);
								laser [i].RollSpeed = 22.5f;
								//laser [i + 8].SetLaser (0.8f, 0.5f, 8, 1.7f);
								//laser [i + 8].RollSpeed = 22.5f;
								laser [i].transform.rotation = Quaternion.Euler (0, 0, i * 45 + 22.5f);
								//laser [i + 8].transform.rotation = Quaternion.Euler (0, 0, i * 45);
								laser [i].ShootLaser ();
						}
						battery [0].prefab = Bullets [11];
						battery [0].SetLoop (1, 0);
						battery [0].SetOval (8, 315);
						battery [0].SetShootRandom (Battery.RELATIVE,10,160);
						battery [1].prefab = Bullets [14];
						battery [1].SetLoop (1, 0);
						battery [1].SetOval (8, 315);
						battery [1].SetShootRandom (Battery.RELATIVE,10,-160);
						break;
				case 8://Tactics_36
						battery [0].prefab = Bullets [12];
						battery [0].SetOval (2, 360);
						battery [0].SetShootDegree (Battery.RELATIVE, 165);
						battery [0].SetLoop (4, 0);
						battery [0].homing = player;
						battery [0].StartLoop (Mover.PLUS, Mover.INFINITY, Mover.DEFAULT);
						for (int i = 0; i < 3; i++) {
								laser [i].SetLaser (0.3f, 1.8f, 8);
						}
						break;
				case 9:
						battery [0].prefab = Bullets [15];
						battery [0].SetShootDegree (Battery.ABSOLUTE, -90);
						battery [0].SetWays (12, 330);
						battery [1].prefab = Bullets [16];
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						battery [1].SetWays (5, 90);
						battery [2].prefab = Bullets [17];
						battery [2].SetShootRandom (Battery.ABSOLUTE, 5, 90);
						battery [2].SetWays (2, 5);
						laser [0].SetLaser (1, 2, 10);
						break;
				}
		}

		public override void Update ()
		{
				switch (enemy._nowPlan) {
				case 1:
						if (CalcTiming (0.5f, 0.1f) <= Time.deltaTime) {
								battery [0].transform.rotation = Battery.LookAt2D (player, battery [1].gameObject);
								battery [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
								battery [1].transform.rotation = Battery.LookAt2D (player, battery [1].gameObject);
								battery [1].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
								StartCoroutine ("Plan1");
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(1,0)) {
								bullet.transform.rotation = Battery.LookAt2D (player, bullet);
								bullet.GetComponent<Bullet> ().move = true;
								bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(0.8f,1)) {
								bullet.transform.rotation = Battery.LookAt2D (player, bullet);
								bullet.GetComponent<Bullet> ().move = true;
								bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
						}
						break;
				case 2:
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(1.1f,0)) {
								bullet.GetComponent<Bullet> ().move = true;
								bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(1f,2)) {
								bullet.GetComponent<Bullet> ().move = true;
								bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
						}
						if (CalcTiming (1.5f, 0.1f) <= Time.deltaTime) {
								battery [1].Shoot ();
						}
						if (CalcTiming (2.5f, 2f) <= Time.deltaTime) {
								battery [1].transform.rotation = Battery.LookAt2D (player, battery [1].gameObject);
								StartCoroutine ("Plan2");
						}
						break;
				case 3:
						if (CalcTiming (0.1f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();

						}
						if (CalcTiming (0.085f, 0) <= Time.deltaTime) {
								battery [1].Shoot ();
								battery [1].Play_SE ();

						}
						break;
				case 4:
						if (CalcTiming (2, 1) <= Time.deltaTime) {
								battery [0].transform.position = player.transform.position;
								battery [0].transform.rotation = Battery.LookAt2D (this.gameObject, player);
								battery [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
								StartCoroutine ("Plan4");
								for (int i = 0; i < 24; i++) {
										battery [1].percentage = 0.04f * i;
										battery [1].CalcPos ();
										battery [1].SetShootDegree (Battery.RELATIVE, 45);
										battery [1].Shoot ();
										battery [1].SetShootDegree (Battery.RELATIVE, -45);
										battery [1].Shoot ();
								}
						}
						foreach (GameObject bullet in pool.GetBulletsByCountEquals(1,0)) {
								bullet.GetComponent<Bullet> ().move = true;
								bullet.GetComponent<Bullet> ().burst (bullet.transform, bullet.GetComponent<Bullet> ().speed);
						}
						break;
				case 5:
						if (CalcTiming (0.07f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						if (CalcTiming (0.04f, 0) <= Time.deltaTime) {
								battery [1].Shoot ();
								battery [1].Play_SE ();
						}
						break;
				case 6:
						if (CalcTiming (2, 0.1f) <= Time.deltaTime) {
								for (int i = 0; i < 5; i++) {
										battery [0].Shoot ();
								}
								battery [0].Play_SE ();
						}
						break;
				case 7:
						battery [0].transform.rotation = laser [4].transform.rotation;
						battery [1].transform.rotation = laser [4].transform.rotation;
						if (CalcTiming (0.7f, 0.5f) <= Time.deltaTime) {
								for (int i = 0; i < 8; i++) {
										battery [0].percentage = i * 0.125f;
										battery [0].CalcPos ();
										battery [0].Shoot ();
										battery [1].percentage = i * 0.125f;
										battery [1].CalcPos ();
										battery [1].Shoot ();
								}
								battery [0].Play_SE ();
						}
						break;
				case 8:
						if (Time.time-nowLoop > 0.5f && CalcTiming (0.07f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						for (int i = 0; i < 3; i++) {
								if (CalcTiming (3, i * 0.3f) <= Time.deltaTime) {
										laser [i].transform.rotation = Battery.LookAt2D (player, laser [i].gameObject);
										laser [i].ShootLaser ();
								}
						}
						break;
				case 9:
						if (CalcTiming (1.5f, 0.5f) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [1].transform.position = new Vector2 (4, 6);
								battery [1].Shoot ();
								battery [1].transform.position = new Vector2 (-4, 6);
								battery [1].Shoot ();
						}
						if (CalcTiming (4.5f, 0.5f) <= Time.deltaTime) {
								laser [0].transform.position = new Vector2 (-4, player.transform.position.y);
								laser [0].ShootLaser ();
								StartCoroutine ("Plan9");
						}
						break;
				}
		}

		IEnumerator Plan1 ()
		{
				for (int i = 0; i < 7; i++) {
						yield return new WaitForSeconds (0.02f);
						battery [0].Shoot ();
						battery [1].Shoot ();
						battery [0].Play_SE ();
				}
				yield break;
		}

		IEnumerator Plan2 ()
		{
				battery [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				for (int i = 0; i < 60; i++) {
						yield return new WaitForSeconds (0.015f);
						battery [0].Shoot ();
						battery [0].Play_SE ();
				}
				battery [0].percentage = 0;
				battery [2].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
				for (int i = 0; i < 60; i++) {
						yield return new WaitForSeconds (0.015f);
						battery [2].Shoot ();
						battery [2].Play_SE ();
				}
				battery [2].percentage = 0;
				yield break;
		}

		IEnumerator Plan3 ()
		{
				for (int i = 0; i < 24; i++) {
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.125f);
				}
				yield break;
		}

		IEnumerator Plan4 ()
		{
				for (int i = 0; i < 60; i++) {
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.015f);
				}
				yield break;
		}

		IEnumerator Plan5 ()
		{
				yield break;
		}

		IEnumerator Plan6 ()
		{
				yield break;
		}

		IEnumerator Plan7 ()
		{
				yield break;
		}

		IEnumerator Plan8 ()
		{
				yield break;
		}

		IEnumerator Plan9(){
				battery [2].SetLine (new Vector2(-5,player.transform.position.y),new Vector2(5,player.transform.position.y));
				yield return new WaitForSeconds (0.8f);
				for (int j = 0; j < 4; j++) {
						for (int i = 0; i < 15; i++) {
								battery [2].percentage = 0.063f * i;
								battery [2].CalcPos ();
								battery [2].Shoot ();
						}
						yield return new WaitForSeconds (0.6f);
				}
				yield break;
		}

		public override void Invoked_Method ()
		{
				switch (enemy._nowPlan) {
				default:

						break;
				}
		}

		public override void TriggerOnWall (GameObject obj)
		{
				switch (enemy._nowPlan) {
				case 6://Tactics_25
						if (obj.GetComponent<Bullet> ().battery_ID == 0) {
								battery [1].transform.position = obj.transform.position;
								battery [1].Shoot ();
								battery [1].Play_SE ();
								obj.GetComponent<Animator> ().SetBool ("Created", false);
						}
						break;
				}
		}

		public override void RaytoWallCollision (Vector2 c_position)
		{

		}
}