﻿using UnityEngine;
using System.Collections;

public class Plan_1_1 : Plan
{
		//----dictionary---
		//switch(enemy._nowPlan){
		//case :
		//
		//break;
		//}
		public override void Init ()
		{
				StopAllCoroutines ();
				base.Init ();
				switch (enemy._nowPlan) {
				case 1://Tactics_1
						battery [0].prefab = Bullets [0];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].SetLoop (1, 0);
						battery [0].SetWays (9, 270);
						break;
				case 2://Tactics_4
						battery [0].prefab = Bullets [1];
						battery [0].SetOval (3, 120);
						battery [0].SetShootRandom (Battery.ABSOLUTE, 45, -90);
						battery [0].SetLoop (1.5f, 0);
						battery [0].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DEFAULT);
						battery [0].transform.Rotate (new Vector3 (0, 0, 90));
						break;
				case 3://Tactics_15
						battery [0].prefab = Bullets [2];
						battery [0].SetLine (new Vector2 (-4, 6), new Vector2 (4, 6));
						battery [0].SetLoop (1, 0);
						battery [0].SetShootDegree (Battery.ABSOLUTE, -90);
						battery [0].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DIM3);
						break;
				case 4://Tactics_6
						battery [0].prefab = Bullets [4];
						battery [0].transform.position = new Vector2 (1.5f, 4.8f);
						battery [0].SetOval (2, 60);
						battery [0].SetLoop (3, 0);
						battery [0].SetShootRolling (1f, 0.5f, 12);
						battery [0].StartLoop (Mover.GO_BACK_PLUS, -1, Mover.DEFAULT);
						battery [1].prefab = Bullets [4];
						battery [1].transform.position = new Vector2 (-1.5f, 4.8f);
						battery [1].transform.Rotate (new Vector3 (0, 0, 180));
						battery [1].SetOval (2, 60);
						battery [1].SetLoop (3, 0);
						battery [1].SetShootRolling (-1f, -0.5f, 12);
						battery [1].StartLoop (Mover.GO_BACK_MINUS, -1, Mover.DEFAULT);
						battery [2].prefab = Bullets [3];
						battery [2].SetShootDegree (Battery.ROCK_ON, 0);
						battery [2].SetWays (9, 120);
						battery [2].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 5://Tactics_7
						battery [0].isWorking = true;
						battery [0].prefab = Bullets [5];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [1].prefab = Bullets [6];
						battery [1].SetOval (0, 27);
						battery [1].SetWays (13, 340);
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						break;
				}
		}

		public override void Update ()
		{
				switch (enemy._nowPlan) {
				case 1:
						if (CalcTiming (0.7f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						break;
				case 2:
						if (CalcTiming (0.1f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						break;
				case 3:
						if (CalcTiming (0.075f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						break;
				case 4:
						if (CalcTiming (2, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan4");
						}
						if (CalcTiming (0.12f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [1].Shoot ();
								battery [1].Play_SE ();
						}
						break;
				case 5:
						if (CalcTiming (2.2f, 0.5f) <= Time.deltaTime) {
								battery [0].Shoot ();
						}
						if (CalcTiming (2.2f, 0.6f) <= Time.deltaTime) {
								battery [1].homing = pool.GetBulletsByCountOver (0, 1, 0) [0];
								StartCoroutine ("Plan5");
								//battery [1].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
						}
						if (nowLoop - Time.time > 2.2f) {
								nowLoop = Time.time;
						}
						break;
				}
		}

		IEnumerator Plan4 ()
		{
//				for (int i = 0; i < 3; i++) {
//						yield return new WaitForSeconds (0.33f);
						battery [2].Shoot ();
						battery [2].Play_SE ();
//				}
				yield break;
		}

		IEnumerator Plan5 ()
		{
				for (int i = 0; i < 4; i++) {
						yield return new WaitForSeconds (0.5f);
						battery [1].SetPosition ();
						battery [1].Shoot ();
						battery [1].Play_SE ();
				}
				yield break;
		}

		public override void RaytoWallCollision (Vector2 c_position)
		{
//		if(CalcTiming(0.2f,0) <= Time.deltaTime){
//			battery [0].transform.position = c_position;
//			battery [0].LoopOneshot ();
//		}
		}
}
