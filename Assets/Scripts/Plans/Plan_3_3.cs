using UnityEngine;
using System.Collections;

public class Plan_3_3 : Plan
{
		//	----dictionary---
		//	switch(enemy._nowPlan){
		//	case :
		//
		//	break;
		//	}
		//	battery[0].prefab = Bullets[0];
		//	battery[0].SetShootDegree (Battery.ROCK_ON,0);
		//	battery[0].SetLoop(2,2,1);
		//	battery[0].StartLoop(Mover.PLUS,-1,Mover.DEFAULT);
		int value;
		public override void Init ()
		{
				base.Init ();
				switch (enemy._nowPlan) {
				case 1://Tactics_2
						battery [0].prefab = Bullets [0];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].SetWays (13, 332);
						battery [1].prefab = Bullets [1];
						battery [1].transform.localPosition = new Vector2 (2, -1);
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						battery [1].SetWays (12, 330);
						battery [2].prefab = Bullets [1];
						battery [2].transform.localPosition = new Vector2 (-2, -1);
						battery [2].SetShootDegree (Battery.ROCK_ON, 0);
						battery [2].SetWays (12, 330);
						break;
				case 2://Tactics_10
						battery [0].transform.localPosition = new Vector2 (0, 0);
						battery [0].prefab = Bullets [2];
						battery [0].SetShootDegree (Battery.ROCK_ON, 180);
						battery [0].SetWays (30, 345);
						battery [1].prefab = Bullets [3];
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						//battery [2].prefab = Bullets [4];
						//battery [2].SetShootRandom (Battery.ROCK_ON, 60, 180);
						battery [1].transform.position = new Vector2 (0, 0);
						//battery [2].transform.position = new Vector2 (0, 0);
						break;
				case 3://Tactics_32
						battery [1].prefab = Bullets [5];
						battery [1].SetShootRandom (Battery.ABSOLUTE, 120, 90);
						battery [1].SetWays (2, 180);
						battery [1].SetLoop (1.6f, 1.2f);
						battery [1].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [0].prefab = Bullets [6];
						battery [0].SetShootRandom (Battery.ABSOLUTE, 120, 90);
						battery [0].SetWays (2, 180);
						battery [0].SetLoop (2, 2);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 4://Tactics_30
						for (int i = 0; i < 6; i++) {
								laser [i].SetLaser (1, 0.5f, 10, 0.8f);
								laser [i].transform.Rotate (new Vector3 (0, 0, i * 60 + 10));
								laser [i].RollSpeed = 30;
								laser [i].ShootLaser ();
						}
						battery [0].prefab = Bullets [7];
						battery [0].SetOval (0.8f, 360);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 5://Tactics_40
						laser [0].SetLaser (0.4f, 1, 10);
						battery [0].prefab = Bullets [8];
						battery [0].SetLoop (1.5f, 0);
						battery [0].SetShootRolling (3, 0, 30);
						break;
				case 6://Tactics_33
						laser [0].SetLaser (0.2f, 0.8f, 10);
						laser [1].SetLaser (0.2f, 0.8f, 10);
						battery [0].prefab = Bullets [9];
						battery [0].transform.position = this.transform.position;
						battery [0].transform.Rotate (0, 0, 180);
						battery [0].SetOval (10, 360);
						battery [0].SetShootDegree (Battery.RELATIVE, 180);
						battery [0].SetLoop (1.6f, 0);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [1].prefab = Bullets [11];
						battery [1].transform.position = this.transform.position;
						battery [1].transform.Rotate (0, 0, 90);
						battery [1].SetOval (7, 360);
						battery [1].SetShootDegree (Battery.RELATIVE, 180);
						battery [1].SetLoop (2f, 0);
						battery [1].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 7://Tactics_39
						laser [2].transform.localPosition = new Vector3 (-3, 1, 0);
						laser [3].transform.localPosition = new Vector3 (1.5f, 0, 0);
						laser [4].transform.localPosition = new Vector3 (-1.5f, 0, 0);
						laser [5].transform.localPosition = new Vector3 (3, 1, 0);
						for (int i = 2; i < 6; i++) {
								laser [i].SetLaser (1f, 3.6f, 10);
						}
						battery [0].prefab = Bullets [10];
						battery [0].SetLoop (1, 0);
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].SetWays (16, 338);
						break;
				case 8:
						battery [0].prefab = Bullets [12];
						battery [0].SetShootDegree (Battery.ABSOLUTE, -90);
						battery [0].SetWays (5, 288);
						battery [1].prefab = Bullets [13];
						battery [1].SetShootDegree (Battery.ABSOLUTE, 90);
						battery [1].SetWays (5, 288);
						for (int i = 0; i < 6; i++) {
								laser [i].SetLaser (0.5f, 0.5f, 10);
						}
						value = 0;
						break;
				}
		}

		IEnumerator Plan1 ()
		{
				yield return new WaitForSeconds (1);
				battery [0].prefab.GetComponent<Bullet> ().speed = 2.5f;
				battery [0].Shoot ();
				battery [0].Play_SE ();
				yield return new WaitForSeconds (0.2f);
				battery [0].prefab.GetComponent<Bullet> ().speed = 2.9f;
				battery [0].Shoot ();
				battery [0].Play_SE ();
				yield return new WaitForSeconds (0.2f);
				battery [0].prefab.GetComponent<Bullet> ().speed = 3.5f;
				battery [0].Shoot ();
				battery [0].Play_SE ();
		}

		IEnumerator Plan2 ()
		{
//				battery [1].transform.position = new Vector2 (0, 0);
//				battery [2].transform.position = new Vector2 (0, 0);
				battery [1].Shoot ();
				for (int i = 0; i < 5; i++) {
						battery [0].prefab.GetComponent<Bullet> ().speed = 2 + i * 0.4f;
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.1f);
				}
				yield break;
		}

		IEnumerator Plan3 ()
		{
				for (int i = 0; i < 4; i++) {
						battery [1].Shoot ();
						battery [1].Play_SE ();
						yield return new WaitForSeconds (0.1f);
				}
				yield break;
		}

		IEnumerator Plan4 ()
		{
				for (int i = 0; i < 3; i++) {
						battery [0].SetShootDegree (Battery.RELATIVE, 45 * i);
						for (int j = 0; j < 16; j++) {
								battery [0].percentage = 0.0625f * j;
								battery [0].CalcPos ();
								battery [0].Shoot ();
						}
						yield return new WaitForSeconds (0.3f);
				}
		}

		IEnumerator Plan5 ()
		{
				for (int i = 0; i < 30; i++) {
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.03f);
				}
				yield break;
		}

		IEnumerator Plan7()
		{
				for (int i = 0; i < 4; i++) {
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.4f);
				}
		}

		public override void Update ()
		{
				switch (enemy._nowPlan) {
				case 1:
						if (CalcTiming (2, 0.5f) <= Time.deltaTime) {
								battery [1].Shoot ();
								battery [2].Shoot ();
								battery [1].Play_SE ();
								StartCoroutine ("Plan1");
						}
						break;
				case 2:
						battery [1].transform.position = new Vector2 (0, 0);
						battery [2].transform.position = new Vector2 (0, 0);
						if (CalcTiming (1.3f, 0.1f) <= Time.deltaTime) {
								StartCoroutine ("Plan2");
						}
						break;
				case 3:
						if (CalcTiming (1.6f, 0.5f) <= Time.deltaTime) {
								StartCoroutine ("Plan3");
						}
						if (CalcTiming (2f, 0.5f) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						break;
				case 4:
						if (CalcTiming (1.6f, 0.5f) <= Time.deltaTime) {
								StartCoroutine ("Plan4");
						}
						break;
				case 5:
						if (CalcTiming (1.8f, 0.5f) <= Time.deltaTime) {
								laser [0].transform.rotation = Battery.LookAt2D (player, laser [0].gameObject);
								laser [0].ShootLaser ();
								battery [0].SetLine (this.transform.position, player.transform.position);
								battery [0].percentage = 0;
								battery [0].StartLoop (Mover.PLUS, 1, Mover.DEFAULT);
								StartCoroutine ("Plan5");
						}
						break;
				case 6:
						if (CalcTiming (2.5f, 0.5f) <= Time.deltaTime) {
								laser [0].transform.rotation = Battery.LookAt2D (player, laser [0].gameObject);
								laser [1].transform.rotation = Battery.LookAt2D (player, laser [1].gameObject);
								laser [0].transform.Rotate (new Vector3 (0, 0, 15));
								laser [1].transform.Rotate (new Vector3 (0, 0, -15));
								laser [0].ShootLaser ();
								laser [1].ShootLaser ();
						}
						if (CalcTiming (0.08f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [0].Play_SE ();
						}
						break;
				case 7:
						if (CalcTiming (4, 0.5f) <= Time.deltaTime) {
								for (int i = 2; i < 6; i++) {
										laser [i].transform.rotation = Battery.LookAt2D (player, laser [i].gameObject);
										laser [i].ShootLaser ();
								}
								battery [0].transform.position = player.transform.position;
						}
						if (CalcTiming (4, 2) <= Time.deltaTime) {
								StartCoroutine ("Plan7");
						}
						break;
				case 8:
						if (CalcTiming (3, 0.1f) <= Time.deltaTime) {
								battery [1].Shoot ();
								battery [0].Shoot ();
								battery [1].Play_SE();
						}
						if (CalcTiming (3, 2f) <= Time.deltaTime) {
								foreach (GameObject bul in pool.GetBulletsByCountOver(1,3,0)) {
										bul.transform.rotation = Battery.LookAt2D (player, bul);
										bul.GetComponent<Bullet> ().burst (bul.transform, 3);
								}
						}
						break;
				}
		}

		public override void TriggerOnWall (GameObject obj)
		{
				switch (enemy._nowPlan) {
				case 8:
						if (obj.GetComponent<Bullet> ().battery_ID == 1) {
								laser[value].transform.position = obj.transform.position;
								laser [value].transform.rotation = Battery.LookAt2D (player, laser [value].gameObject);
								laser [value].ShootLaser ();
								obj.GetComponent<Animator> ().SetBool ("Created", false);
								value++;
								if (value > 5) {
										value = 0;
								}
						}
						break;
				}
		}

		public override void RaytoWallCollision (Vector2 c_position)
		{

		}
}
