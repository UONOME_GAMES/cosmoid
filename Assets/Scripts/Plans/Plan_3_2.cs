using UnityEngine;
using System.Collections;

public class Plan_3_2 : Plan
{
		//----dictionary---
		//switch(enemy._nowPlan){
		//case :
		//
		//break;
		//}
		float val;
		public override void Init ()
		{
				base.Init ();
				switch (enemy._nowPlan) {
				case 1://Tactics_3
						battery [0].prefab = Bullets [0];
						battery [0].SetOval (0.1f, 342);
						battery [0].SetLoop (1, 0);
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [1].prefab = Bullets [1];
						battery [1].transform.localPosition = new Vector2 (2, 0);
						battery [1].SetShootDegree (Battery.ROCK_ON, 0);
						battery [1].SetOval (0.6f, 360);
						battery [1].SetWays (7, 308.5f);
						battery [1].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [2].prefab = Bullets [1];
						battery [2].transform.localPosition = new Vector2 (-2, 0);
						battery [2].SetShootDegree (Battery.ROCK_ON, 0);
						battery [2].SetOval (0.6f, 360);
						battery [2].SetWays (7, 308.5f);
						break;
				case 2://Tactics_8
						for (int i = 0; i < 3; i++) {
								mover [i].transform.Rotate (new Vector3 (0, 0, i * 120));
								mover [i].SetOval (3.7f, 360);
								mover [i].SetLoop (5.5f, 0);
								mover [i].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
								battery [i].isWorking = true;
						}
						battery [0].prefab = Bullets [3];
						battery [0].homing = mover [0].position;
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].SetWays (2, 30);
						battery [1].prefab = Bullets [4];
						battery [1].homing = mover [1].position;
						battery [1].SetShootDegree (Battery.RELATIVE, 0);
						battery [2].prefab = Bullets [2];
						battery [2].homing = mover [2].position;
						battery [2].SetShootDegree (Battery.ABSOLUTE, -90);
						break;
				case 3://Tactics_9
						for (int i = 0; i < 4; i++) {
								mover [i].transform.Rotate (new Vector3 (0, 0, i * 90));
								mover [i].SetOval (1, 342);
								mover [i].SetLoop (2f, 0);
								mover [i].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						}
						for (int i = 0; i < 3; i += 2) {
								battery [i].prefab = Bullets [5];
								battery [i].homing = mover [i].position;
								battery[i].SetLoop (0.6f,0);
								battery [i + 1].prefab = Bullets [6];
								battery [i + 1].homing = mover [i + 1].position;
								battery [i + 1].SetWays (3, 50);
								battery [i + 1].SetShootDegree (Battery.ROCK_ON, 0);
								battery[i + 1].SetLoop(1.5f,0);
								battery [i + 1].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						}
						battery [0].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						battery [2].StartLoop (Mover.MINUS, -1, Mover.DEFAULT);
						break;
				case 4://Tactics_11
						battery [0].prefab = Bullets [7];
						battery [0].SetLoop (0.9f, 0);
						battery [0].SetShootRolling (3f, 0, 30);
						break;
				case 5://Tactics_19
						battery [0].prefab = Bullets [8];
						battery [0].transform.position = enemy.transform.position;
						battery [0].SetHypocycloid (1.3f, 6);
						battery [0].SetShootDegree (Battery.RELATIVE, 15);
						battery [0].SetLoop (1.8f, 1.1f);
						battery [1].prefab = Bullets [9];
						battery [1].SetLine (new Vector2 (-5, 6), new Vector2 (5, 6));
						battery [1].SetShootDegree (Battery.ABSOLUTE, -90);
						battery [1].SetLoop (1,0);
						//battery [1].SetSpeed (2, 3.5f);
						//battery [1].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
						break;
				case 6:
						for (int i = 0; i < 4; i++) {
								laser [i].SetLaser (1.5f, 1.8f, 8, 0.1f);
								laser [i].RollSpeed = 24;
								laser [i].transform.Rotate (new Vector3 (0, 0, i * 90));
						}
						battery [0].prefab = Bullets [10];
						battery [0].SetShootDegree (Battery.ROCK_ON, 0);
						battery [0].SetWays (3, 90);
						break;
				case 7:
						for (int i = 0; i < 3; i++) {
								mover [i].transform.Rotate (new Vector3 (0, 0, i * 120));
								mover [i].SetOval (3.7f, 360);
								mover [i].SetLoop (5, 0);
								mover [i].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
								battery [i].prefab = Bullets [11];
								battery [i].homing = mover [0].position;
								battery [i].SetOval (0.8f, 324);
								battery [i].SetShootDegree (Battery.RELATIVE, i*120);
								battery [i].SetLoop (2.5f, 0);
								battery [i].StartLoop (Mover.PLUS, -1, Mover.DEFAULT);
								battery [i].transform.Rotate (new Vector3 (0, 0, i * 120));
						}
						for (int i = 0; i < 4; i++) {
								battery [i + 3].prefab = Bullets [12];
								battery [i + 3].transform.position = mover [1].position.transform.position;
								battery [i + 3].SetLoop (5, 0);
								battery [i + 3].SetOval (0.8f, 360);
								battery [i + 3].SetShootDegree (Battery.ABSOLUTE, -90);
								battery [i + 3].StartLoop (Mover.MINUS, -1, Mover.DEFAULT);
								battery [i + 3].transform.Rotate (new Vector3 (0, 0, i * 90));
						}
						laser [0].SetLaser (1, 1, 3, 0.1f);
						laser [0].RollSpeed = 36;
						laser [0].transform.position = mover [2].position.transform.position;
						laser [0].transform.parent = mover [2].position.transform;
						laser [1].SetLaser (1, 1, 3, 0.1f);
						laser [1].RollSpeed = 36;
						laser [1].transform.Rotate (new Vector3 (0, 0, 180));
						laser [1].transform.position = mover [2].position.transform.position;
						laser [1].transform.parent = mover [2].position.transform;
						break;
				}
		}

		IEnumerator Plan1 ()
		{
				battery [1].Shoot ();
				battery [1].Play_SE ();
				yield return new WaitForSeconds (0.2f);
				foreach (GameObject bullet in pool.GetBulletsByCountEquals(0.2f,1)) {
						bullet.transform.rotation = Battery.LookAt2D (player, bullet);
						bullet.GetComponent<Bullet> ().burst (bullet.transform, 2.5f);
				}
				yield return new WaitForSeconds (0.3f);
				battery [2].Shoot ();
				battery [2].Play_SE ();
				yield return new WaitForSeconds (0.2f);
				foreach (GameObject bullet in pool.GetBulletsByCountEquals(0.2f,2)) {
						bullet.transform.rotation = Battery.LookAt2D (player, bullet);
						bullet.GetComponent<Bullet> ().burst (bullet.transform, 2.5f);
				}
				yield break;
		}

		IEnumerator Plan2 ()
		{
				for (int i = 0; i < 30; i++) {
						yield return new WaitForSeconds (0.05f);
						battery [0].Shoot ();
						battery [0].Play_SE ();
				}
				yield break;
		}

		IEnumerator Plan4 ()
		{
				for (int i = 0; i < 45; i++) {
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.0125f);
				}
				yield break;
		}

		IEnumerator Plan5 ()
		{
				for (int i = 0; i < 20; i++) {
						battery [0].percentage = 0.05f * i;
						//Debug.Log (battery [0].percentage);
						battery [0].CalcPos ();
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.01f);
				}
				for (int i = 0; i < 20; i++) {
						battery [0].percentage = 0.975f - 0.05f * i;
						//Debug.Log (battery [0].percentage);
						battery [0].CalcPos ();
						battery [0].Shoot ();
						battery [0].Play_SE ();
						yield return new WaitForSeconds (0.01f);
				}
				float rnd = Random.Range (0, 45);
				foreach (GameObject bul in pool.GetBulletsByCountOver(0.03f,1f,0)) {
						bul.GetComponent<Bullet> ().move = true;
						bul.transform.Rotate (new Vector3 (0, 0, rnd - 22.5f));
						bul.GetComponent<Bullet> ().burst (bul.transform, bul.GetComponent<Bullet> ().speed);
				}
				yield break;
		}

		public override void Update ()
		{
				switch (enemy._nowPlan) {
				case 1:
						if (CalcTiming (0.05f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
						}
						if (CalcTiming (1, 0.1f) <= Time.deltaTime) {
								StartCoroutine ("Plan1");
						}
						break;
				case 2:
						for (int i = 0; i < 3; i++) {
								battery [i].SetPosition ();
						}
						if (CalcTiming (0.1f, 0) <= Time.deltaTime) {
								battery [1].Shoot ();
						}
						if (CalcTiming (0.13f, 0) <= Time.deltaTime) {
								battery [2].Shoot ();
						}
						if (CalcTiming (2.6f, 0.1f) <= Time.deltaTime) {
								StartCoroutine ("Plan2");
						}
						break;
				case 3:
						if (CalcTiming (0.1f, 0) <= Time.deltaTime) {
								battery [0].Shoot ();
								battery [2].Shoot ();
								battery [0].Play_SE ();
						}
						if (CalcTiming (1.5f, 0) <= Time.deltaTime) {
								battery [1].Shoot ();
								battery [3].Shoot ();
								battery [1].Play_SE ();
						}
						break;
				case 4:
						foreach (GameObject bul in pool.GetBulletsByCountEquals(2,0)) {
								bul.GetComponent<Bullet> ().move = true;
								bul.GetComponent<Bullet> ().burst (bul.transform, bul.GetComponent<Bullet> ().speed);
						}
						if (CalcTiming (1.9f, 0) <= Time.deltaTime) {
								battery [0].SetLine (this.transform.position, player.transform.position);
								battery [0].StartLoop (Mover.PLUS, 1, Mover.DIM2_EASE_OUT);
								StartCoroutine ("Plan4");
						}
						break;
				case 5:
						if (CalcTiming (1.8f, 0) <= Time.deltaTime) {
								StartCoroutine ("Plan5");
						}
						if (CalcTiming (0.7f, 0) <= Time.deltaTime) {
								val = Random.Range (1, 25) / 1000;
								for (int i = 0; i < 20; i++) {
										battery [1].prefab.GetComponent<Bullet> ().speed = 1.5f + Mathf.PingPong (i * 0.2f, 1);
										battery [1].percentage = 0.05f * i + val;
										battery [1].CalcPos ();
										battery [1].Shoot ();
								}
								battery [1].Play_SE ();
						}
						break;
				case 7:
						for (int i = 0; i < 4; i++) {
								battery [i + 3].transform.position = mover [1].position.transform.position;
						}
						if (CalcTiming (0.1f, 0) <= Time.deltaTime) {
								for (int i = 0; i < 3; i++) {
										battery [i].Shoot ();
								}
								battery [0].Play_SE ();
						}
						if (CalcTiming (0.25f, 0) <= Time.deltaTime) {
								for (int i = 0; i < 4; i++) {
										battery [i + 3].Shoot ();
								}
								battery [3].Play_SE ();
						}
						break;
				}
		}

		public override void RaytoWallCollision (Vector2 c_position)
		{
				if (CalcTiming (0.125f, 0) <= Time.deltaTime) {
						battery [0].transform.position = c_position;
						battery [0].Shoot ();
						battery [0].Play_SE ();
				}
		}
}